# Ticket Me

A ticket system made with React, Redux, styled components and MirageJS as a mock backend.

![Screenshot1](screenshots/homepage.png)
<!-- blank line -->
![Screenshot2](screenshots/ticket.png)
<!-- blank line -->
![Screenshot3](screenshots/new.png)
