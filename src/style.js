import styled, { createGlobalStyle } from 'styled-components';
import { FaCircleNotch } from 'react-icons/fa';

export const Wrapper = styled.div`
    height: 100%;
    position: relative;
    overflow: hidden;
`;

export const Container = styled.div`
    overflow-y: scroll;
    display: grid;
    grid-template-areas: 'menu main panel';
    grid-template-columns: ${props =>
        props.theme.sidebarCollapsed ? '0fr auto 0fr' : '220px auto 0fr'};
    height: 100%;
`;

export const LoadingApp = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const LoadingIcon = styled(FaCircleNotch)`
    @keyframes rotation {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(359deg);
        }
    }
    animation: rotation 1s infinite linear;
    color: #2d2e40;
`;

export default createGlobalStyle`
    *{
        box-sizing: border-box;
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    *:focus{outline:none !important;}

    html, body, #root {
        height: 100%;
    }

    body {
        background: #1b1b25;
        color: #fff;
        font-family: 'Open Sans', sans-serif;
    }

    body.showTicketsPanel #ticketsPanel {
        transform: translate3d(0,0,0);
        transition: transform 0.4s;
        transition-timing-function: cubic-bezier(0.7,0,0.3,1);
    }
`;
