import React, { useEffect } from 'react';
import {
    HashRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { Wrapper, LoadingApp, LoadingIcon, Container } from './style';
import TicketList from './components/TicketList';
import { onLoad } from './state/actions';
import LockedTicketsPanel from './components/LockedTicketsPanel';

export default function App() {
    const dispatch = useDispatch();
    const appLoaded = useSelector(state => state.ui.common.appLoaded);
    const collapsed = useSelector(state => state.ui.common.sidebarCollapsed);

    const theme = {
        sidebarCollapsed: collapsed,
    };

    useEffect(() => {
        dispatch(onLoad(null));
    }, [dispatch]);

    return (
        <Router>
            {appLoaded ? (
                <ThemeProvider theme={theme}>
                    <Wrapper>
                        <LockedTicketsPanel id='ticketsPanel' />
                        <Container id='container'>
                            <Switch>
                                <Redirect exact from='/' to='/tickets' />
                                <Route path='/tickets'>
                                    <TicketList />
                                </Route>
                            </Switch>
                        </Container>
                    </Wrapper>
                </ThemeProvider>
            ) : (
                <LoadingApp>
                    <LoadingIcon size='5em' />
                </LoadingApp>
            )}
        </Router>
    );
}
