import React, { useEffect, useState } from 'react';
import { createSelector } from 'reselect';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment-twitter';
import { Route, useRouteMatch, useHistory, Switch } from 'react-router-dom';
import { getFilteredEntities, getIsFetching } from '../../state/reducers';
import {
    UpdateStatusFilter,
    StatusFilter,
    UpdateSortBy,
    SortBy,
    SortByOptions,
    fetchTickets,
    unlockTickets,
    lockTickets,
    fetchLockedTickets,
} from '../../state/actions';
import Dropdown from '../../shared/Dropdown';
import UserPreview from '../../shared/UserPreview';
import PriorityLabel from '../../shared/PriorityLabel';
import LockButton from '../../shared/LockButton';
import { FaLockOpen, FaLock } from 'react-icons/fa';
import RouterButton from '../../shared/RouterButton';
import TicketModal from '../../shared/TicketModal';
import RouterLink from '../../shared/RouterLink';
import SearchButton from '../SearchButton';
import Ticket from '../Ticket';
import TicketComposer from '../TicketComposer';
import {
    StyledList,
    TopInfo,
    Title,
    Filters,
    FilterGroup,
    LastUpdated,
    ClockIcon,
    PlusIcon,
    FilterIcon,
    SortIcon,
    StyledTable,
    StatusLabel,
    StatusIcon,
    NumTicketsLocked,
    MyTicketsButton,
} from './style';

import { FaTicketAlt } from 'react-icons/fa';

const filterTickets = createSelector(
    [state => state.entities.tickets, state => state.ui.tickets.statusFilter],
    (tickets, status) => getFilteredEntities(tickets, status)
);

export default function TicketList() {
    const dispatch = useDispatch();
    const match = useRouteMatch();
    const history = useHistory();
    const tickets = useSelector(filterTickets);
    const [searchResults, setSearchResults] = useState([]);
    const status = useSelector(state => state.ui.tickets.statusFilter);
    const isFetching = useSelector(state =>
        getIsFetching(state.entities.tickets, status)
    );
    const sortBy = useSelector(state => state.ui.tickets.sortBy);
    const currentUser = useSelector(state => state.ui.common.currentUser);
    const lockedTickets = useSelector(state => state.ui.tickets.lockedTickets);
    const [searchKeywords, setSearchKeywords] = useState('');

    useEffect(() => {
        if (searchKeywords === '') setSearchResults([]);
        const results = tickets.filter(
            ticket =>
                ticket.subject
                    .toUpperCase()
                    .indexOf(searchKeywords.toUpperCase()) > -1
        );
        setSearchResults(results);
    }, [searchKeywords, tickets]);

    const columns = React.useMemo(
        () => [
            {
                title: 'id',
                accessor: 'id',
                sortOrder: false,
                transformValue: value => `#${value}`,
            },
            {
                title: 'subject',
                accessor: 'subject',
                onClick: () =>
                    dispatch(
                        UpdateSortBy(
                            sortBy === SortBy.AZ ? SortBy.ZA : SortBy.AZ
                        )
                    ),
                sortOrder:
                    sortBy === SortBy.AZ || sortBy === SortBy.ZA
                        ? SortByOptions[sortBy].order
                        : false,
                className: 'pointer',
                transformValue: (value, ticket) => (
                    <RouterLink to={`${match.url}/${ticket.id}`}>
                        {value}
                    </RouterLink>
                ),
            },
            {
                title: 'Locked By',
                accessor: 'lockedBy',
                sortOrder: false,
                transformValue: (lockedBy, ticket) => (
                    <LockButton lockedBy={lockedBy} ticket={ticket} />
                ),
            },
            {
                title: 'Last Updated',
                accessor: 'lastUpdated',
                onClick: () =>
                    dispatch(
                        UpdateSortBy(
                            sortBy === SortBy.NEWEST
                                ? SortBy.OLDEST
                                : SortBy.NEWEST
                        )
                    ),
                sortOrder:
                    sortBy === SortBy.NEWEST || sortBy === SortBy.OLDEST
                        ? SortByOptions[sortBy].order
                        : false,
                transformValue: value => (
                    <LastUpdated>
                        <ClockIcon /> {moment(value).twitter()}
                    </LastUpdated>
                ),
                className: 'pointer',
            },
            {
                title: 'assignee',
                accessor: 'assignedTo',
                transformValue: id => <UserPreview fullName={false} id={id} />,
                sortOrder: false,
            },
            {
                title: 'priority',
                accessor: 'priority',
                transformValue: id => <PriorityLabel id={id} />,
                sortOrder: false,
            },
        ],
        [dispatch, sortBy, match.url]
    );

    useEffect(() => {
        dispatch(fetchLockedTickets(currentUser));
        dispatch(fetchTickets(status));
    }, [status, dispatch, currentUser]);

    const multiSelectionMenu = React.useMemo(
        () => [
            {
                content: (
                    <>
                        <FaLockOpen size={20} /> Unlock
                    </>
                ),
                onClick: items => dispatch(unlockTickets(items)),
            },
            {
                content: (
                    <>
                        <FaLock size={17} /> Lock
                    </>
                ),
                onClick: items => dispatch(lockTickets(items, currentUser)),
            },
        ],
        [dispatch, currentUser]
    );

    const rowClasses = row => {
        const { lockedBy } = row;
        return lockedBy.userId === currentUser ? 'locked' : '';
    };

    const handleMyTClick = e => {
        e.preventDefault();
        e.stopPropagation();
        const body = document.body;
        body.classList.add('showTicketsPanel');
    };

    return (
        <StyledList>
            <TopInfo>
                <MyTicketsButton onClick={handleMyTClick}>
                    <FaTicketAlt />
                    {lockedTickets.length > 0 ? (
                        <NumTicketsLocked>
                            {lockedTickets.length}
                        </NumTicketsLocked>
                    ) : null}
                </MyTicketsButton>
                <Title>
                    Tickets{' '}
                    <StatusLabel>
                        <StatusIcon size='0.6em' />
                        {searchKeywords !== ''
                            ? 'Searching'
                            : status.toLowerCase()}
                    </StatusLabel>
                </Title>
                <RouterButton
                    to={`${match.path}/composer`}
                    style={{ marginLeft: 'auto' }}>
                    <PlusIcon />
                    <span>New ticket</span>
                </RouterButton>

                <Filters>
                    <FilterGroup>
                        <SearchButton setKeywords={setSearchKeywords} />
                    </FilterGroup>
                    <FilterGroup>
                        <Dropdown
                            selected={sortBy}
                            options={Object.keys(SortBy)}
                            onChange={el => dispatch(UpdateSortBy(el))}>
                            <SortIcon /> Sort
                        </Dropdown>
                    </FilterGroup>
                    <FilterGroup>
                        <Dropdown
                            selected={status}
                            options={Object.keys(StatusFilter)}
                            onChange={el => dispatch(UpdateStatusFilter(el))}>
                            <FilterIcon /> Filter
                        </Dropdown>
                    </FilterGroup>
                </Filters>
            </TopInfo>

            <StyledTable
                multiSelectionMenu={multiSelectionMenu}
                data={searchKeywords.length > 0 ? searchResults : tickets}
                columns={columns}
                sortBy={SortByOptions[sortBy]}
                isFetching={isFetching && !tickets.length}
                rowClasses={rowClasses}
            />

            <Switch>
                <Route
                    path={`${match.path}/composer`}
                    render={() => (
                        <TicketModal
                            el={TicketComposer}
                            width='800px'
                            closeModal={() => history.push(match.url)}
                        />
                    )}
                />

                <Route
                    path={`${match.path}/:ticketId`}
                    render={routeProps => (
                        <TicketModal
                            el={Ticket}
                            id={routeProps.match.params.ticketId}
                            closeModal={() => history.push(match.url)}
                        />
                    )}
                />
            </Switch>
        </StyledList>
    );
}
