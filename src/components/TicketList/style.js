import styled from 'styled-components';
import Table from '../../shared/Table';
import {
    FaClock,
    FaPlus,
    FaFilter,
    FaStream,
    FaChevronRight,
} from 'react-icons/fa';

export const StyledList = styled.div`
    grid-area: main;
    height: 100%;
    background: #1b1b25;
    border-right: 1px solid rgba(81, 81, 116, 0.25);
    padding: 30px 20px 30px;
    position: relative;
    z-index: 1;
`;

export const TopInfo = styled.div`
    display: flex;
    align-items: center;
    justify-content: left;
    margin: 0 20px 30px 20px;
`;

export const Title = styled.div`
    font-size: 27px;
    font-weight: 400;
    display: flex;
    align-items: center;
`;

export const Filters = styled.div`
    display: flex;
`;
export const FilterGroup = styled.div`
    display: flex;
    align-items: center;
    margin-left: 25px;
`;

export const LastUpdated = styled.div`
    display: flex;
    align-items: center;
    font-weight: 600;
`;

export const ClockIcon = styled(FaClock)`
    margin-right: 5px;
    fill: #fff;
`;

export const PlusIcon = styled(FaPlus)`
    margin-right: 7px;
`;

export const FilterIcon = styled(FaFilter)`
    margin-right: 7px;
`;

export const SortIcon = styled(FaStream)`
    margin-right: 7px;
`;

export const StyledTable = styled(Table)`
    .row {
        .column {
            font-weight: 500;
        }

        .column-id {
            color: #89899f;
        }

        .column-subject {
            font-weight: 600;
            line-height: 20px;
            padding: 0 10px 0 0;
            cursor: pointer;
            flex: 1;
            min-width: 0;
            span {
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
        }

        &.locked {
            .column-lockedBy {
                button {
                    border: 1px solid #cc5600;
                }
            }
        }
    }
`;

export const StatusLabel = styled.span`
    color: #a9aabf;
    display: flex;
    align-items: baseline;
    text-transform: capitalize;
`;

export const StatusIcon = styled(FaChevronRight)`
    margin: 0 5px 0 10px;
`;

export const MyTicketsButton = styled.div`
    display: flex;
    align-items: center;
    border: none;
    background: #252531;
    height: 36px;
    line-height: 36px;
    border-radius: 4px;
    padding: 0 15px;
    color: #fff;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    font-size: 14px;
    opacity: 0.8;
    cursor: pointer;
    user-select: none;
    position: relative;
    margin-right: 20px;

    svg {
        flex-shrink: 0;
    }

    &:hover {
        opacity: 1;
    }

    &:active {
        opacity: 0.65;
    }
`;

export const NumTicketsLocked = styled.span`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 21px;
    height: 21px;
    border-radius: 21px;
    margin-left: auto;
    background: #cc5600;
    color: #fff;
    font-weight: bold;
    font-size: 11px;
    position: absolute;
    right: -8px;
    top: -8px;
`;
