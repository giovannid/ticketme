import styled from 'styled-components';
import LockButton from '../../shared/LockButton';
import RouterLink from '../../shared/RouterLink';
import Button from '../../shared/Button';

export const StyledLockedTicketsPanel = styled.div`
    position: absolute;
    z-index: 1001;
    width: 300px;
    height: 100%;
    transform: translate3d(-320px, 0, 0);
    transition: transform 0.4s;
    transition-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
    display: flex;
    flex-direction: column;
    background: #1b1b25;
    box-shadow: 0 2px 4px 0 rgba(34, 36, 38, 0.12),
        0 2px 10px 0 rgba(34, 36, 38, 0.15);
    width: 300px;
    user-select: none;
`;

export const Heading = styled.div`
    padding: 10px;
    margin: 0;
    border-bottom: 1px solid rgba(81, 81, 116, 0.25);
    font-size: 17px;
    display: flex;
    align-items: center;
`;

export const List = styled.div`
    overflow-y: scroll;
`;

export const Item = styled.div`
    display: flex;
    align-items: center;
    cursor: pointer;
    user-select: none;
    border-bottom: 1px solid rgba(81, 81, 116, 0.25);
    padding: 0 10px;

    &:last-child {
        border-bottom: none;
    }
`;

export const StyledRouterLink = styled(RouterLink)`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 12px;
    padding: 0;
    cursor: pointer;
    height: 50px;
    line-height: 50px;
    display: block;
    width: 100%;
`;

export const Id = styled.span`
    color: #89899f;
    font-weight: 500;
    margin-right: 10px;
    font-size: 12px;
`;

export const StyledUnlocked = styled(LockButton)`
    margin-left: auto;
`;

export const Empty = styled.div`
    display: flex;
    flex-direction: column;
    padding: 30px;
    color: #6a6a7d;
    align-items: center;

    svg {
        margin-bottom: 10px;
    }
`;

export const ActionButton = styled(Button)`
    margin-left: auto;
    font-size: 14px;
    font-weight: 600;

    svg {
        padding-right: 8px;
        font-size: 20px;
    }

    &.close {
        padding: 0 10px;

        svg {
            padding-right: 0;
        }
    }
`;
