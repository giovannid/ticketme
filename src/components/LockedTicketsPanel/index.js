import React, { useEffect, useRef } from 'react';
import { createSelector } from 'reselect';
import { useSelector } from 'react-redux';
import { getEntitiesFromArr } from '../../state/reducers';
import { FaSadTear, FaTimes } from 'react-icons/fa';
import {
    StyledLockedTicketsPanel,
    Heading,
    List,
    Item,
    StyledRouterLink,
    Id,
    StyledUnlocked,
    Empty,
    ActionButton,
} from './style';

const lockedTicketsList = createSelector(
    [
        state => state.entities.tickets.byId,
        state => state.ui.tickets.lockedTickets,
    ],
    (tickets, ids) => getEntitiesFromArr(tickets, ids)
);

export default function LockedTicketsPanel({ className, id }) {
    const lockedTickets = useSelector(lockedTicketsList);
    const panel = useRef(null);

    const closePanel = () => {
        const body = document.body;
        body.classList.remove('showTicketsPanel');
    };

    useEffect(() => {
        function detectOutsideClick(e) {
            if (panel.current !== null && !panel.current.contains(e.target)) {
                closePanel();
            }
        }
        window.addEventListener('click', detectOutsideClick);
        return () => window.removeEventListener('click', detectOutsideClick);
    }, []);

    const handleItemClick = e => {
        e.stopPropagation();
        closePanel();
    };

    return (
        <StyledLockedTicketsPanel className={className} id={id} ref={panel}>
            <Heading>
                <span>My Tickets</span>
                <ActionButton className='close' onClick={closePanel}>
                    <FaTimes />
                </ActionButton>
            </Heading>

            <List>
                {lockedTickets.map(ticket => (
                    <Item key={ticket.id} onClick={handleItemClick}>
                        <StyledRouterLink to={`/tickets/${ticket.id}`}>
                            <Id>#{ticket.id}</Id>
                            {ticket.subject}
                        </StyledRouterLink>
                        <StyledUnlocked
                            lockedBy={ticket.lockedBy}
                            ticket={ticket}
                            userPreview={false}
                        />
                    </Item>
                ))}
                {lockedTickets.length === 0 ? (
                    <Empty>
                        <FaSadTear size={22} /> No tickets locked.
                    </Empty>
                ) : null}
            </List>
        </StyledLockedTicketsPanel>
    );
}
