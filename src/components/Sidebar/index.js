import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { collapseSidebar } from '../../state/actions';
import {
    FaTachometerAlt,
    FaCog,
    FaChartBar,
    FaUserFriends,
    FaRegLifeRing,
    FaRegComments,
    FaTicketAlt,
    FaBell,
    FaCaretDown,
    FaAngleDoubleLeft,
    FaAngleDoubleRight,
} from 'react-icons/fa';
import { fetchLockedTickets } from '../../state/actions';
import {
    StyledSidebar,
    Section,
    SectionTitle,
    ItemContainer,
    NumTicketsLocked,
    StyledLockedTicketsPanel,
    StyledLink,
    FixedButton,
    StyledIcon,
    DisabledLink,
    Text,
    User,
    UserOptions,
    CustomUserPreview,
    MoreIcon,
    Notification,
} from './style';

export default function Menu() {
    const collapsed = useSelector(state => state.ui.common.sidebarCollapsed);
    const lockedTickets = useSelector(state => state.ui.tickets.lockedTickets);
    const currentUser = useSelector(state => state.ui.common.currentUser);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchLockedTickets(currentUser));
    }, [dispatch, currentUser]);

    const handleCollapseClick = () => dispatch(collapseSidebar(!collapsed));

    return (
        <StyledSidebar>
            <Section>
                <User>
                    <UserOptions>
                        <CustomUserPreview
                            id={currentUser}
                            firstNameOnly={true}
                        />
                        <MoreIcon>
                            <FaCaretDown />
                        </MoreIcon>
                    </UserOptions>
                    <Notification>
                        <FaBell size='1.3em' />
                    </Notification>
                </User>
            </Section>
            <Section>
                <SectionTitle>Overview</SectionTitle>
                <DisabledLink>
                    <StyledIcon>
                        <FaTachometerAlt />
                    </StyledIcon>
                    <Text className='hideOnHover'>Dashboard</Text>
                    <Text className='showOnHover'>Not Implemented</Text>
                </DisabledLink>
                <ItemContainer>
                    <StyledLink activeClassName='selected' to='/tickets'>
                        <StyledIcon>
                            <FaTicketAlt />
                        </StyledIcon>
                        <Text>Tickets</Text>
                        {lockedTickets.length > 0 ? (
                            <NumTicketsLocked>
                                {lockedTickets.length}
                            </NumTicketsLocked>
                        ) : null}
                    </StyledLink>
                    <StyledLockedTicketsPanel />
                </ItemContainer>
            </Section>
            <Section>
                <SectionTitle>Help</SectionTitle>
                <DisabledLink>
                    <StyledIcon>
                        <FaRegLifeRing />
                    </StyledIcon>
                    <Text className='hideOnHover'>Knowledge Base</Text>
                    <Text className='showOnHover'>Not Implemented</Text>
                </DisabledLink>
                <DisabledLink>
                    <StyledIcon>
                        <FaRegComments />
                    </StyledIcon>
                    <Text className='hideOnHover'>Community</Text>
                    <Text className='showOnHover'>Not Implemented</Text>
                </DisabledLink>
            </Section>
            <Section>
                <SectionTitle>Administration</SectionTitle>
                <DisabledLink>
                    <StyledIcon>
                        <FaUserFriends />
                    </StyledIcon>
                    <Text className='hideOnHover'>Users</Text>
                    <Text className='showOnHover'>Not Implemented</Text>
                </DisabledLink>
                <DisabledLink>
                    <StyledIcon>
                        <FaChartBar />
                    </StyledIcon>
                    <Text className='hideOnHover'>Reports</Text>
                    <Text className='showOnHover'>Not Implemented</Text>
                </DisabledLink>
                <DisabledLink>
                    <StyledIcon>
                        <FaCog />
                    </StyledIcon>
                    <Text className='hideOnHover'>Configuration</Text>
                    <Text className='showOnHover'>Not Implemented</Text>
                </DisabledLink>
            </Section>
            <Section className='noMargin'>
                <FixedButton onClick={handleCollapseClick} id='collapseButton'>
                    <StyledIcon className='close'>
                        <FaAngleDoubleLeft />
                    </StyledIcon>
                    <StyledIcon className='open'>
                        <FaAngleDoubleRight />
                    </StyledIcon>
                    <Text>Collapse</Text>
                </FixedButton>
            </Section>
        </StyledSidebar>
    );
}
