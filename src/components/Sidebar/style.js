import styled from 'styled-components';
import UserPreview from '../../shared/UserPreview';
import Button from '../../shared/Button';
import LockedTicketsPanel from '../LockedTicketsPanel';
import { NavLink } from 'react-router-dom';

export const StyledSidebar = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    grid-area: menu;
    background: #21212c;
    border-right: 1px solid rgba(81, 81, 116, 0.25);
    width: ${props => (props.theme.sidebarCollapsed ? '70px' : '220px')};
    height: 100%;
    z-index: 2;
`;

export const Section = styled.div`
    margin: 20px 0;

    &.noMargin {
        margin: 0;
    }
`;

export const SectionTitle = styled.h3`
    color: rgb(110, 110, 136);
    font-size: 11px;
    text-transform: uppercase;
    font-weight: 700;
    color: #6a6a7d;
    margin: 0;
    padding: 0 15px 10px;
    display: ${props => (props.theme.sidebarCollapsed ? 'none' : 'initial')};
    user-select: none;
`;

export const ItemContainer = styled.div`
    position: relative;
    z-index: 99;
`;

export const NumTicketsLocked = styled.span`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 21px;
    height: 21px;
    border-radius: 21px;
    margin-left: auto;
    background: #cc5600;
    color: #fff;
    font-weight: bold;
    font-size: 11px;

    position: ${props =>
        props.theme.sidebarCollapsed ? 'absolute' : 'initial'};
    left: ${props => (props.theme.sidebarCollapsed ? '44px' : 'initial')};
    top: ${props => (props.theme.sidebarCollapsed ? '5px' : 'initial')};
`;

export const StyledLockedTicketsPanel = styled(LockedTicketsPanel)`
    display: none;

    ${ItemContainer}:hover & {
        display: initial;
    }

    position: absolute;
    top: 0;
    left: ${props => (props.theme.sidebarCollapsed ? '69px' : '219px')};
    z-index: 99;
`;

export const StyledLink = styled(NavLink)`
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 14px;
    font-weight: 500;
    color: #fff;
    margin: 5px 0;
    padding: 10px 15px;
    user-select: none;
    justify-content: ${props =>
        props.theme.sidebarCollapsed ? 'center' : 'initial'};
    position: relative;

    &.selected {
        background: #313240;
        color: #cc5600;
    }

    &:hover {
        background: #313240;
    }
`;

export const FixedButton = styled.button`
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 14px;
    font-weight: 500;
    color: #fff;
    padding: 10px 15px;
    user-select: none;
    width: ${props => (props.theme.sidebarCollapsed ? '70px' : '220px')};
    position: absolute;
    bottom: 0;
    border-radius: 0;
    border: 0;
    cursor: pointer;
    background: none;
    justify-content: ${props =>
        props.theme.sidebarCollapsed ? 'center' : 'initial'};

    span {
        pointer-events: none;
    }

    &:hover {
        background: #313240;
    }

    & .close {
        display: ${props => (props.theme.sidebarCollapsed ? 'none' : 'flex')};
    }

    & .open {
        display: ${props => (props.theme.sidebarCollapsed ? 'flex' : 'none')};
    }
`;

export const StyledIcon = styled.span`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 30px;
    height: 30px;
    background: #292a37;
    border-radius: 4px;
    margin-right: ${props => (props.theme.sidebarCollapsed ? '0' : '20px')};

    ${StyledLink}.selected & {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    }
`;

export const DisabledLink = styled.span`
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 14px;
    font-weight: 500;
    color: #fff;
    margin: 5px 0;
    padding: 10px 15px;
    user-select: none;
    justify-content: ${props =>
        props.theme.sidebarCollapsed ? 'center' : 'initial'};

    &:hover {
        cursor: not-allowed;
    }
`;

export const Text = styled.span`
    display: ${props => (props.theme.sidebarCollapsed ? 'none' : 'initial')};

    &.showOnHover {
        display: none;
        background: #313240;
        border-radius: 4px;
        height: 30px;
        padding: 0 12px;
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 600;
        align-items: center;
    }

    ${DisabledLink}:hover & {
        &.showOnHover {
            display: ${props =>
                props.theme.sidebarCollapsed ? 'none' : 'flex'};
        }

        &.hideOnHover {
            display: none;
        }
    }
`;

export const User = styled.div`
    margin: 0 15px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const UserOptions = styled(Button)`
    background: #292a37;
    height: 40px;
    width: 100%;
    max-width: 135px;
    margin-right: 10px;
    justify-content: space-between;
    opacity: 1;
    display: ${props => (props.theme.sidebarCollapsed ? 'none' : 'flex')};

    &:hover {
        cursor: pointer;
        background: #313240;
    }
`;

export const CustomUserPreview = styled(UserPreview)`
    max-width: 105px;
    flex-shrink: 1;
`;

export const MoreIcon = styled.div`
    display: flex;
    align-items: center;
    flex-shrink: 0;
`;

export const Notification = styled(Button)`
    background: #292a37;
    height: 40px;
    padding: 0 14px;
    opacity: 1;

    &:hover {
        cursor: pointer;
        background: #313240;
    }
`;
