import React, { useState, useEffect } from 'react';
import { FaSearch, FaTimes } from 'react-icons/fa';
import {
    StyledSearchButton,
    Button,
    InputWrapper,
    Input,
    InputIcon,
} from './style';

export default function SearchButton({ className, setKeywords }) {
    const [isFocused, setIsFocused] = useState(false);
    const [value, setValue] = useState('');

    const toggleInput = () => setIsFocused(!isFocused);
    const handleInputChange = e => setValue(e.target.value);

    useEffect(() => {
        if (isFocused === false) {
            setKeywords('');
            setValue('');
        }
    }, [isFocused, setKeywords]);

    useEffect(() => {
        const timeoutId = setTimeout(() => setKeywords(value), 10);
        return () => clearTimeout(timeoutId);
    }, [value, setKeywords]);

    return (
        <StyledSearchButton className={className}>
            {!isFocused ? (
                <Button onClick={toggleInput}>
                    <FaSearch />
                </Button>
            ) : (
                <InputWrapper>
                    <Input
                        value={value}
                        onChange={handleInputChange}
                        autoFocus
                    />
                    <InputIcon onClick={toggleInput}>
                        <FaTimes />
                    </InputIcon>
                </InputWrapper>
            )}
        </StyledSearchButton>
    );
}
