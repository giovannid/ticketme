import styled from 'styled-components';

export const StyledSearchButton = styled.div``;

export const Button = styled.button`
    display: flex;
    align-items: center;
    border: none;
    background: #252531;
    height: 36px;
    line-height: 36px;
    border-radius: 4px;
    padding: 0 15px;
    color: #fff;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    font-size: 14px;
    opacity: 0.8;
    cursor: pointer;
    user-select: none;

    &:hover {
        opacity: 1;
    }

    &:active {
        opacity: 0.65;
    }
`;

export const InputWrapper = styled.div`
    position: relative;
`;

export const Input = styled.input`
    width: 250px;
    display: block;
    border: none;
    height: 36px;
    line-height: 36px;
    font-size: 15px;
    color: #fff;
    border-radius: 4px;
    background: #252531;
    padding: 0 41px 0 10px;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
`;

export const InputIcon = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 36px;
    width: 44px;
    position: absolute;
    right: 0;
    top: 0;
    cursor: pointer;
    font-size: 17px;
`;
