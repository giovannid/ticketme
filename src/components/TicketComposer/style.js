import styled from 'styled-components';
import Button from '../../shared/Button';

export const StyledTicketComposer = styled.div`
    width: 800px;
    padding: 25px 30px 40px;

    .elementContainer {
        width: 100%;
    }
`;

export const Header = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0 0 30px 0;
`;

export const Actions = styled.div`
    display: flex;
    justify-content: flex-end;
`;

export const ActionButton = styled(Button)`
    margin-left: 20px;
    font-size: 14px;
    font-weight: 600;

    svg {
        font-size: 20px;
    }

    &.submit {
        background: #954a13;
    }
`;

export const Title = styled.h2`
    color: #fff;
    margin: 0;
    font-weight: 400;
`;

export const Status = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin: 0 0 25px;

    .statusDropdown {
        margin-bottom: 0;
    }
`;

export const Label = styled.div`
    font-weight: 700;
    color: #6a6a7d;
    font-size: 12px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

export const Subject = styled.div`
    margin: 0 0 25px;
    div {
        color: ${props => (props.error ? 'red' : 'inherit')};
    }
`;

export const SubjectField = styled.input`
    width: 100%;
    display: block;
    border: none;
    height: 36px;
    line-height: 36px;
    font-size: 15px;
    color: #fff;
    border-radius: 4px;
    background: #252531;
    padding: 0 41px 0 10px;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);

    &::placeholder {
        color: red;
    }
`;

export const Message = styled.div`
    margin: 0 0 25px;

    div {
        color: ${props => (props.error ? 'red' : 'inherit')};
    }
`;

export const MessageField = styled.textarea`
    width: 100%;
    height: 150px;
    border: none;
    border-radius: 4px;
    background: #252531;
    padding: 10px;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    resize: none;
    color: #fff;

    &::placeholder {
        color: red;
    }
`;

export const Assignee = styled.div``;
export const Priority = styled.div``;
