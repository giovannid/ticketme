import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import {
    createTicket,
    StatusFilter,
    UserType,
    PriorityList,
} from '../../state/actions';
import { getFilteredEntities, getIsFetching } from '../../state/reducers';
import DropdownWithSearch from '../../shared/DropdownWithSearch';
import FilterOption from '../../shared/FilterOption';
import PriorityLabel from '../../shared/PriorityLabel';
import UserPreview from '../../shared/UserPreview';
import { FaTimes } from 'react-icons/fa';
import {
    StyledTicketComposer,
    Header,
    Actions,
    ActionButton,
    Title,
    Status,
    Label,
    Subject,
    SubjectField,
    Message,
    MessageField,
    Assignee,
    Priority,
} from './style';

const filterUsers = createSelector(
    state => state.entities.users,
    users => getFilteredEntities(users, UserType.STAFF)
);

export default function TicketComposer({ closeModal }) {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.ui.common.currentUser);
    const staff = useSelector(filterUsers);
    const isFetchingStaff = useSelector(state =>
        getIsFetching(state.entities.users, UserType.STAFF)
    );

    const [fields, setFields] = useState({
        subject: {
            error: false,
            value: '',
        },
        message: {
            error: false,
            value: '',
        },
        status: StatusFilter.OPEN,
        assignedTo: currentUser,
        priority: 4,
    });

    const handleSubmit = e => {
        e.preventDefault();
        e.stopPropagation();

        if (fields.subject.value === '' || fields.message.value === '') {
            setFields({
                ...fields,
                message: {
                    error: true,
                    value: '',
                },
                subject: {
                    error: true,
                    value: '',
                },
            });
        } else {
            dispatch(
                createTicket({
                    user: currentUser,
                    ticket: {
                        subject: fields.subject.value,
                        status: fields.status,
                        priority: fields.priority,
                        assignedTo: fields.assignedTo,
                    },
                    thread: {
                        body: fields.message.value,
                    },
                })
            );
            closeModal();
        }
    };

    return (
        <StyledTicketComposer>
            <Header>
                <Title>Create Ticket</Title>
                <ActionButton className='close' onClick={() => closeModal()}>
                    <FaTimes />
                </ActionButton>
            </Header>
            <Status>
                <DropdownWithSearch
                    label={'Status'}
                    className='statusDropdown'
                    accessor='id'
                    searchAttr='id'
                    defaultOption={StatusFilter.OPEN}
                    onChange={value => setFields({ ...fields, status: value })}
                    options={Object.keys(StatusFilter)
                        .slice(0, 4)
                        .map(id => ({ id }))}
                    isFetching={false}
                    element={FilterOption}
                    readOnly={false}
                />
            </Status>

            <Subject error={fields.subject.error}>
                <Label>Subject</Label>
                <SubjectField
                    placeholder={fields.message.error ? 'Required field' : ''}
                    value={fields.subject.value}
                    onChange={e =>
                        setFields({
                            ...fields,
                            subject: {
                                ...fields.subject,
                                error: false,
                                value: e.target.value,
                            },
                        })
                    }
                />
            </Subject>

            <Message error={fields.message.error}>
                <Label>Message</Label>
                <MessageField
                    placeholder={fields.message.error ? 'Required field' : ''}
                    value={fields.message.value}
                    onChange={e =>
                        setFields({
                            ...fields,
                            message: {
                                ...fields.message,
                                error: false,
                                value: e.target.value,
                            },
                        })
                    }
                />
            </Message>

            <Assignee>
                <DropdownWithSearch
                    label={'Assignee'}
                    accessor='id'
                    searchAttr='fullName'
                    defaultOption={currentUser}
                    onChange={value =>
                        setFields({ ...fields, assignedTo: value })
                    }
                    options={staff}
                    isFetching={isFetchingStaff}
                    element={UserPreview}
                    readOnly={false}
                />
            </Assignee>

            <Priority>
                <DropdownWithSearch
                    label={'Priority'}
                    accessor='id'
                    searchAttr='label'
                    defaultOption={4}
                    onChange={value =>
                        setFields({
                            ...fields,
                            priority: parseInt(value),
                        })
                    }
                    options={Object.keys(PriorityList).map(id => ({
                        id,
                        label: PriorityList[id],
                    }))}
                    isFetching={false}
                    element={PriorityLabel}
                    readOnly={false}
                />
            </Priority>

            <Actions>
                <ActionButton className='submit' onClick={handleSubmit}>
                    Create
                </ActionButton>
                <ActionButton onClick={() => closeModal()}>Cancel</ActionButton>
            </Actions>
        </StyledTicketComposer>
    );
}
