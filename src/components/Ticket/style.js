import styled from 'styled-components';
import Button from '../../shared/Button';
import UserPreview from '../../shared/UserPreview';

export const StyledTicket = styled.div`
    width: 100%;
    padding: 25px 30px 40px;
    background: #1b1b25;
`;

export const StyledLoadingIcon = styled.div`
    width: 100%;
    padding: 25px 30px 40px;
`;

export const Header = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const Id = styled.div`
    color: #89899f;
    display: flex;
    align-items: center;
    font-weight: 600;
    font-size: 15px;
`;

export const Status = styled.div`
    display: flex;
    align-items: center;
    margin-right: auto;
    margin-left: 20px;

    .statusDropdown {
        margin-bottom: 0;
    }
`;

export const Actions = styled.div`
    display: flex;
`;

export const ActionButton = styled(Button)`
    margin-left: 20px;
    font-size: 14px;
    font-weight: 600;

    svg {
        padding-right: 8px;
        font-size: 20px;
    }

    &.close {
        padding: 0 10px;

        svg {
            padding-right: 0;
        }
    }
`;

export const Body = styled.div`
    margin-top: 15px;
    display: flex;
`;
export const Content = styled.div`
    width: 70%;
    padding-right: 50px;
`;

export const Subject = styled.h2`
    margin: 0;
    word-break: break-word;
`;
export const Sidebar = styled.div`
    width: 30%;
`;

export const Info = styled.div`
    font-size: 13px;
    display: inline-block;
    width: 100%;
    margin-bottom: 15px;
`;

export const Label = styled.div`
    font-weight: 700;
    color: #6a6a7d;
    font-size: 12px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

export const Text = styled.div`
    color: #89899f;
    font-size: 12px;
    font-weight: 600;
`;

export const Form = styled.div`
    width: 100%;
    display: flex;
    margin: 30px 0;
    align-items: flex-start;
`;

export const ProfilePic = styled(UserPreview)`
    margin-right: 13px;
    .avatar {
        margin-right: 0;
    }
`;

export const Formfields = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
`;

export const Textarea = styled.textarea`
    width: 100%;
    height: 150px;
    border: none;
    border-radius: 4px;
    background: #252531;
    padding: 15px;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    resize: none;
    color: #fff;
    font-size: 14px;
    margin-bottom: 10px;

    &::placeholder {
        color: #89899f;
    }
`;

export const Reply = styled(Button)`
    opacity: 1;
    background: #954a13;
`;
