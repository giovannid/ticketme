import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FaPen, FaTimes, FaSave, FaTrash } from 'react-icons/fa';
import Thread from '../../shared/Thread';
import DropdownWithSearch from '../../shared/DropdownWithSearch';
import UserPreview from '../../shared/UserPreview';
import { getFilteredEntities, getIsFetching } from '../../state/reducers';
import { createSelector } from 'reselect';
import {
    UserType,
    StatusFilter,
    PriorityList,
    fetchUsers,
    replyTicket,
    updateTicket,
    deleteTicket,
    fetchTicket,
} from '../../state/actions';
import FilterOption from '../../shared/FilterOption';
import PriorityLabel from '../../shared/PriorityLabel';
import LoadingTicket from '../../shared/LoadingTicket';
import moment from 'moment/moment';
import * as mtwitter from 'moment-twitter';
import LockButton from '../../shared/LockButton';
import {
    StyledTicket,
    StyledLoadingIcon,
    Header,
    Id,
    Status,
    Actions,
    ActionButton,
    Body,
    Content,
    Subject,
    Sidebar,
    Info,
    Label,
    Text,
    Form,
    ProfilePic,
    Formfields,
    Textarea,
    Reply,
} from './style';

const filterUsers = createSelector(
    state => state.entities.users,
    users => getFilteredEntities(users, UserType.STAFF)
);

export default function Ticket({ id, className, closeModal }) {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.ui.common.currentUser);
    const ticket = useSelector(state => state.entities.tickets.byId[id]);
    const staff = useSelector(filterUsers);
    const isFetchingStaff = useSelector(state =>
        getIsFetching(state.entities.users, UserType.STAFF)
    );

    useEffect(() => {
        dispatch(fetchUsers(UserType.STAFF));
        dispatch(fetchTicket(id));
    }, [dispatch, id]);

    const [textareaText, setTextareaText] = useState('');

    const handleTextareaChange = e => {
        setTextareaText(e.target.value);
    };

    const handleReplyClick = () => {
        if (textareaText.length > 0) {
            dispatch(replyTicket(currentUser, ticket.id, textareaText));
            setTextareaText('');
        }
    };

    const [editMode, setEditMode] = useState(false);
    const [changes, setChanges] = useState({});

    const handleEditClick = e => {
        e.preventDefault();
        e.stopPropagation();
        setEditMode(true);
    };

    const handleSaveClick = e => {
        e.preventDefault();
        e.stopPropagation();
        if (Object.keys(changes).length > 0) {
            dispatch(updateTicket(ticket.id, changes));
        }
        setEditMode(false);
    };

    const handleTrashClick = e => {
        e.preventDefault();
        e.stopPropagation();
        dispatch(deleteTicket(ticket.id));
        closeModal();
    };

    if (!ticket)
        return (
            <StyledLoadingIcon>
                <LoadingTicket />
            </StyledLoadingIcon>
        );

    return (
        <StyledTicket className={className}>
            <Header>
                <Id>#{ticket.id}</Id>
                <Status>
                    <DropdownWithSearch
                        className='statusDropdown'
                        accessor='id'
                        searchAttr='id'
                        selected={ticket.status}
                        defaultOption={StatusFilter.OPEN}
                        onChange={value =>
                            setChanges({ ...changes, status: value })
                        }
                        options={Object.keys(StatusFilter)
                            .slice(0, 4)
                            .map(id => ({ id }))}
                        isFetching={false}
                        element={FilterOption}
                        readOnly={!editMode}
                    />
                </Status>
                <Actions>
                    {editMode ? (
                        <>
                            <ActionButton onClick={handleTrashClick}>
                                <FaTrash />
                                Delete
                            </ActionButton>
                            <ActionButton onClick={handleSaveClick}>
                                <FaSave />
                                Save
                            </ActionButton>
                        </>
                    ) : (
                        <ActionButton onClick={handleEditClick}>
                            <FaPen />
                            Edit
                        </ActionButton>
                    )}
                    <ActionButton
                        className='close'
                        onClick={() => closeModal()}>
                        <FaTimes />
                    </ActionButton>
                </Actions>
            </Header>
            <Body>
                <Content>
                    <Subject>{ticket.subject}</Subject>

                    {ticket.lockedBy.userId === currentUser ? (
                        <Form>
                            <ProfilePic
                                width={32}
                                height={32}
                                id={currentUser}
                                displayName={false}
                            />
                            <Formfields>
                                <Textarea
                                    placeholder='Type your reply here...'
                                    value={textareaText}
                                    onChange={handleTextareaChange}
                                />
                                <Reply onClick={handleReplyClick}>Reply</Reply>
                            </Formfields>
                        </Form>
                    ) : null}

                    {ticket.threads
                        .slice()
                        .reverse()
                        .map(id => (
                            <Thread key={id} id={id} />
                        ))}
                </Content>
                <Sidebar>
                    <Info>
                        <Label>Locked By</Label>
                        <LockButton
                            lockedBy={ticket.lockedBy}
                            ticket={ticket}
                            userPreview={true}
                            displayName={true}
                        />
                    </Info>

                    <DropdownWithSearch
                        label={'Assignee'}
                        accessor='id'
                        searchAttr='fullName'
                        selected={ticket.assignedTo}
                        defaultOption={currentUser}
                        onChange={value =>
                            setChanges({ ...changes, assignedTo: value })
                        }
                        options={staff}
                        isFetching={isFetchingStaff}
                        element={UserPreview}
                        readOnly={!editMode}
                    />

                    <DropdownWithSearch
                        label={'Priority'}
                        accessor='id'
                        searchAttr='label'
                        defaultOption={4}
                        selected={ticket.priority}
                        onChange={value =>
                            setChanges({
                                ...changes,
                                priority: parseInt(value),
                            })
                        }
                        options={Object.keys(PriorityList).map(id => ({
                            id,
                            label: PriorityList[id],
                        }))}
                        isFetching={false}
                        element={PriorityLabel}
                        readOnly={!editMode}
                    />

                    <Info>
                        <Label>Created At</Label>
                        <Text title={moment(ticket.createdAt).toString()}>
                            {mtwitter(ticket.createdAt).twitter()} ago{' '}
                        </Text>
                    </Info>

                    <Info>
                        <Label>Last Updated</Label>
                        <Text title={moment(ticket.lastUpdated).toString()}>
                            {mtwitter(ticket.lastUpdated).twitter()} ago
                        </Text>
                    </Info>
                </Sidebar>
            </Body>
        </StyledTicket>
    );
}
