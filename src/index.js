import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './state/store';
import { initialState } from './state/initialState';
import Style from './style';
import Normalize from './normalize';
import App from './App';

require('./server');

const store = configureStore(initialState);
const rootElement = document.getElementById('root');

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Normalize />
            <Style />
            <App />
        </Provider>
    </React.StrictMode>,
    rootElement
);
