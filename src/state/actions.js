import * as api from '../api';
import { getEntitiesFromArr } from './reducers';

export const REPLY_TICKET_SUCCESS = 'REPLY_TICKET_SUCCESS';
export const replyTicket = (userId, ticketId, body) => dispatch =>
    api.replyTicket(userId, ticketId, body).then(response => {
        dispatch({
            type: REPLY_TICKET_SUCCESS,
            response,
        });
    });

export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';
export const fetchUsers = userType => dispatch => {
    dispatch({
        type: FETCH_USERS_REQUEST,
        userType,
    });

    return api.fetchUsers(userType).then(
        response => {
            dispatch({
                type: FETCH_USERS_SUCCESS,
                response,
                userType,
            });
        },
        error => {
            dispatch({
                type: FETCH_USERS_FAILURE,
                message: error.message || 'Something went wrong!',
            });
        }
    );
};

export const FETCH_SINGLE_TICKET_SUCCESS = 'FETCH_SINGLE_TICKET_SUCCESS';
export const fetchTicket = id => dispatch =>
    api.fetchTicket(id).then(response => {
        dispatch({
            type: FETCH_SINGLE_TICKET_SUCCESS,
            response,
        });
    });

export const FETCH_TICKETS_REQUEST = 'FETCH_TICKETS_REQUEST';
export const FETCH_TICKETS_SUCCESS = 'FETCH_TICKETS_SUCCESS';
export const FETCH_TICKETS_FAILURE = 'FETCH_TICKETS_FAILURE';
export const fetchTickets = filter => dispatch => {
    dispatch({
        type: FETCH_TICKETS_REQUEST,
        filter,
    });

    return api.fetchTickets(filter).then(
        response => {
            dispatch({
                type: FETCH_TICKETS_SUCCESS,
                response,
                filter,
            });
        },
        error => {
            dispatch({
                type: FETCH_TICKETS_FAILURE,
                message: error.message || 'Something went wrong!',
            });
        }
    );
};

export const FETCH_LOCKED_TICKETS_REQUEST = 'FETCH_LOCKED_TICKETS_REQUEST';
export const FETCH_LOCKED_TICKETS_SUCCESS = 'FETCH_LOCKED_TICKETS_SUCCESS';
export const FETCH_LOCKED_TICKETS_FAILURE = 'FETCH_LOCKED_TICKETS_FAILURE';
export const fetchLockedTickets = userId => dispatch => {
    dispatch({
        type: FETCH_LOCKED_TICKETS_REQUEST,
        userId,
    });

    return api.fetchLockedTickets(userId).then(
        response => {
            dispatch({
                type: FETCH_LOCKED_TICKETS_SUCCESS,
                response,
            });
        },
        error => {
            dispatch({
                type: FETCH_LOCKED_TICKETS_FAILURE,
                message: error.message || 'Something went wrong!',
            });
        }
    );
};

export const CREATE_TICKET_SUCCESS = 'CREATE_TICKET_SUCCESS';
export const createTicket = data => dispatch =>
    api.createTicket(data).then(response => {
        dispatch({
            type: CREATE_TICKET_SUCCESS,
            response,
        });
    });

export const UPDATE_TICKET_SUCCESS = 'UPDATE_TICKET_SUCCESS';
export const updateTicket = (id, data) => dispatch =>
    api.updateTicket(id, data).then(response => {
        dispatch({
            type: UPDATE_TICKET_SUCCESS,
            response,
        });
    });

export const DELETE_TICKET_SUCCESS = 'DELETE_TICKET_SUCCESS';
export const deleteTicket = id => dispatch =>
    api.deleteTicket(id).then(response => {
        dispatch({
            type: DELETE_TICKET_SUCCESS,
            response,
        });
    });

export const LOCK_TICKETS_SUCCESS = 'LOCK_TICKETS_SUCCESS';
export const lockTickets = (arrIds, user) => (dispatch, getState) => {
    const { entities } = getState();
    const tickets = getEntitiesFromArr(entities.tickets.byId, arrIds);
    const unlockedTickets = tickets.reduce((finalVal, ticket) => {
        if (ticket.lockedBy === false) return [...finalVal, ticket.id];
        return finalVal;
    }, []);
    if (unlockedTickets.length === 0) return;

    return api.lockTickets(unlockedTickets, user).then(response => {
        dispatch({
            type: LOCK_TICKETS_SUCCESS,
            response,
        });
    });
};

export const UNLOCK_TICKETS_SUCCESS = 'UNLOCK_TICKETS_SUCCESS';
export const unlockTickets = arrIds => (dispatch, getState) => {
    const { entities } = getState();
    const tickets = getEntitiesFromArr(entities.tickets.byId, arrIds);
    const onlyLocked = tickets.reduce((finalVal, ticket) => {
        if (ticket.lockedBy !== false) return [...finalVal, ticket.id];
        return finalVal;
    }, []);
    if (onlyLocked.length === 0) return;

    api.unlockTickets(onlyLocked).then(response => {
        dispatch({
            type: UNLOCK_TICKETS_SUCCESS,
            response,
        });
    });
};

export const UPDATE_STATUS_FILTER = 'UPDATE_STATUS_FILTER';
export const UpdateStatusFilter = filter => ({
    type: UPDATE_STATUS_FILTER,
    filter,
});

export const UPDATE_SORTBY = 'UPDATE_SORTBY';
export const UpdateSortBy = sortBy => ({
    type: UPDATE_SORTBY,
    sortBy,
});

export const APP_LOAD = 'APP_LOAD';
export const onLoad = token => dispatch =>
    api.currentUser(token).then(response => {
        dispatch({
            type: APP_LOAD,
            token,
            response,
        });
    });

export const COLLAPSE_SIDEBAR = 'COLLAPSE_SIDEBAR';
export const collapseSidebar = value => dispatch =>
    dispatch({
        type: COLLAPSE_SIDEBAR,
        closed: value,
    });

export const StatusFilter = {
    UNANSWERED: 'UNANSWERED',
    ANSWERED: 'ANSWERED',
    CLOSED: 'CLOSED',
    OPEN: 'OPEN',
    ALL: 'ALL',
};

export const SortBy = {
    AZ: 'AZ',
    ZA: 'ZA',
    NEWEST: 'NEWEST',
    OLDEST: 'OLDEST',
};

export const SortByOptions = {
    AZ: { key: 'subject', order: 'asc', asString: true },
    ZA: { key: 'subject', order: 'desc', asString: true },
    NEWEST: { key: 'lastUpdated', order: 'asc' },
    OLDEST: { key: 'lastUpdated', order: 'desc' },
};

export const PriorityList = {
    1: 'CRITICAL',
    2: 'IMPORTANT',
    3: 'NORMAL',
    4: 'LOW',
};

export const UserType = {
    STAFF: 'STAFF',
    USER: 'USER',
};
