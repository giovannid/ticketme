import { combineReducers } from 'redux';
import ui from './ui';
import entities from './entities';

const rootReducer = combineReducers({
    entities,
    ui,
});

export default rootReducer;

const getIds = state => state.ids;
const getEntity = (state, id) => state[id];

export const getEntitiesFromArr = (state, arr) =>
    arr.map(id => getEntity(state, id));

export const getIsFetching = (state, filter) =>
    state.idsByFilter[filter].isFetching;

export const getErrorMessage = (state, filter) =>
    state.idsByFilter[filter].errorMessage;

export const getFilteredEntities = (state, filter) => {
    const ids = getIds(state.idsByFilter[filter]);
    return getEntitiesFromArr(state.byId, ids);
};
