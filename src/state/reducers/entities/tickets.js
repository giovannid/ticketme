import { combineReducers } from 'redux';
import {
    FETCH_TICKETS_REQUEST,
    FETCH_TICKETS_SUCCESS,
    FETCH_SINGLE_TICKET_SUCCESS,
    FETCH_TICKETS_FAILURE,
    CREATE_TICKET_SUCCESS,
    REPLY_TICKET_SUCCESS,
    UPDATE_TICKET_SUCCESS,
    DELETE_TICKET_SUCCESS,
    StatusFilter,
} from '../../actions';

const replyHelper = (state, action, filter) => {
    const ticketId = action.response.result.ticket;
    switch (filter) {
        case StatusFilter.ANSWERED:
            return state.includes(ticketId) ? state : [...state, ticketId];
        case StatusFilter.CLOSED:
        case StatusFilter.OPEN:
        case StatusFilter.UNANSWERED:
            return state.filter(id => id !== ticketId);
        default:
            return state;
    }
};

const updateHelper = (state, action, filter) => {
    const id = action.response.result.ticket;
    const ticket = action.response.entities.tickets[id];
    if (filter === StatusFilter.ALL) return state;
    if (filter === ticket.status) {
        return state.includes(id) ? state : [...state, id];
    } else {
        return state.filter(sid => sid !== id);
    }
};

const fetchHelper = (state, action, filter) => {
    const id = action.response.result.ticket;
    const ticket = action.response.entities.tickets[id];
    if (filter === ticket.status) {
        return state.includes(id) ? state : [...state, id];
    }
    return state;
};

const addHelper = (state, action, filter) => {
    const id = action.response.result.ticket;
    const ticket = action.response.entities.tickets[id];
    switch (filter) {
        case ticket.status:
        case StatusFilter.ALL:
            return [...state, action.response.result.ticket];
        default:
            return state;
    }
};

const createList = filter => {
    const ids = (state = [], action) => {
        if (!action.response) return state;

        switch (action.type) {
            case FETCH_TICKETS_SUCCESS:
                return filter === action.filter
                    ? action.response.result.tickets
                    : state;
            case CREATE_TICKET_SUCCESS:
                return addHelper(state, action, filter);
            case REPLY_TICKET_SUCCESS:
                return replyHelper(state, action, filter);
            case UPDATE_TICKET_SUCCESS:
                return updateHelper(state, action, filter);
            case DELETE_TICKET_SUCCESS:
                return state.filter(id => id !== action.response.result.ticket);
            case FETCH_SINGLE_TICKET_SUCCESS:
                return fetchHelper(state, action, filter);
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        if (filter !== action.filter) {
            return state;
        }
        switch (action.type) {
            case FETCH_TICKETS_REQUEST:
                return true;
            case FETCH_TICKETS_SUCCESS:
            case FETCH_TICKETS_FAILURE:
                return false;
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        if (filter !== action.filter) {
            return state;
        }
        switch (action.type) {
            case FETCH_TICKETS_REQUEST:
            case FETCH_TICKETS_SUCCESS:
                return null;
            case FETCH_TICKETS_FAILURE:
                return action.message;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
    });
};

const byId = (state = {}, action) => {
    if (action.response) {
        switch (action.type) {
            case DELETE_TICKET_SUCCESS:
                const {
                    [action.response.result.ticket]: ticket,
                    ...rest
                } = state;
                return rest;
            default:
                return {
                    ...state,
                    ...action.response.entities.tickets,
                };
        }
    }
    return state;
};

const idsByFilter = combineReducers({
    [StatusFilter.UNANSWERED]: createList(StatusFilter.UNANSWERED),
    [StatusFilter.ANSWERED]: createList(StatusFilter.ANSWERED),
    [StatusFilter.CLOSED]: createList(StatusFilter.CLOSED),
    [StatusFilter.OPEN]: createList(StatusFilter.OPEN),
    [StatusFilter.ALL]: createList(StatusFilter.ALL),
});

const tickets = combineReducers({
    byId,
    idsByFilter,
});

export default tickets;
