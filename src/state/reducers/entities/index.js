import { combineReducers } from 'redux';
import tickets from './tickets';
import users from './users';
import threads from './threads';

const entities = combineReducers({
    tickets,
    users,
    threads,
});

export default entities;
