import { combineReducers } from 'redux';
import { DELETE_TICKET_SUCCESS } from '../../actions';

const deleteHelper = (state, action) => {
    return Object.keys(state).reduce(
        (z, a) =>
            action.response.result.threads.includes(a)
                ? z
                : { ...z, [a]: state[a] },
        {}
    );
};

const byId = (state = {}, action) => {
    if (action.response) {
        switch (action.type) {
            case DELETE_TICKET_SUCCESS:
                return deleteHelper(state, action);
            default:
                return {
                    ...state,
                    ...action.response.entities.threads,
                };
        }
    }
    return state;
};

const threads = combineReducers({
    byId,
});

export default threads;
