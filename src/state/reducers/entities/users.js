import { combineReducers } from 'redux';
import {
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    FETCH_TICKETS_SUCCESS,
    FETCH_LOCKED_TICKETS_SUCCESS,
    APP_LOAD,
    UserType,
} from '../../actions';

const createList = userType => {
    const ids = (state = [], action) => {
        switch (action.type) {
            case FETCH_USERS_SUCCESS:
                return userType === action.userType
                    ? action.response.result.users
                    : state;
            case APP_LOAD:
                const user = action.response.result.user;
                return !state.includes(user) &&
                    action.response.entities.users[user].type === userType
                    ? [...state, user]
                    : state;
            case FETCH_TICKETS_SUCCESS:
            case FETCH_LOCKED_TICKETS_SUCCESS:
                return [
                    ...state,
                    ...(action.response.result.users
                        ? action.response.result.users.filter(
                              id =>
                                  !state.includes(id) &&
                                  action.response.entities.users[id].type ===
                                      userType
                          )
                        : []),
                ];
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        if (userType !== action.userType) {
            return state;
        }
        switch (action.type) {
            case FETCH_USERS_REQUEST:
                return true;
            case FETCH_USERS_SUCCESS:
            case FETCH_USERS_FAILURE:
                return false;
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        if (userType !== action.userType) {
            return state;
        }
        switch (action.type) {
            case FETCH_USERS_REQUEST:
            case FETCH_USERS_SUCCESS:
                return null;
            case FETCH_USERS_FAILURE:
                return action.message;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
    });
};

const byId = (state = {}, action) => {
    if (action.response) {
        return {
            ...state,
            ...action.response.entities.users,
        };
    }
    return state;
};

const idsByFilter = combineReducers({
    [UserType.STAFF]: createList(UserType.STAFF),
    [UserType.USER]: createList(UserType.USER),
});

const users = combineReducers({
    byId,
    idsByFilter,
});

export default users;
