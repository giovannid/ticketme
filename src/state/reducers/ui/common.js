import { APP_LOAD, COLLAPSE_SIDEBAR } from '../../actions';

const common = (state = {}, action) => {
    switch (action.type) {
        case APP_LOAD:
            return {
                ...state,
                token: action.token || null,
                appLoaded: true,
                currentUser: action.response
                    ? action.response.result.user
                    : null,
            };
        case COLLAPSE_SIDEBAR:
            return {
                ...state,
                sidebarCollapsed: action.closed,
            };
        default:
            return state;
    }
};
export default common;
