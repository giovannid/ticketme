import { combineReducers } from 'redux';
import tickets from './tickets';
import common from './common';

const ui = combineReducers({
    common,
    tickets,
});

export default ui;
