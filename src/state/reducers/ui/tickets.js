import {
    UPDATE_STATUS_FILTER,
    UPDATE_SORTBY,
    LOCK_TICKETS_SUCCESS,
    UNLOCK_TICKETS_SUCCESS,
    FETCH_LOCKED_TICKETS_SUCCESS,
    REPLY_TICKET_SUCCESS,
    DELETE_TICKET_SUCCESS,
} from '../../actions';

const tickets = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_STATUS_FILTER:
            return {
                ...state,
                statusFilter: action.filter,
            };
        case UPDATE_SORTBY:
            return {
                ...state,
                sortBy: action.sortBy,
            };
        case LOCK_TICKETS_SUCCESS:
            return {
                ...state,
                lockedTickets: [
                    ...state.lockedTickets,
                    ...action.response.result.tickets,
                ],
            };
        case UNLOCK_TICKETS_SUCCESS:
            return {
                ...state,
                lockedTickets: state.lockedTickets.filter(
                    id => !action.response.result.tickets.includes(id)
                ),
            };
        case FETCH_LOCKED_TICKETS_SUCCESS:
            return {
                ...state,
                lockedTickets: action.response.result.tickets,
            };
        case REPLY_TICKET_SUCCESS:
        case DELETE_TICKET_SUCCESS:
            return {
                ...state,
                lockedTickets: state.lockedTickets.filter(
                    id => id !== action.response.result.ticket
                ),
            };
        default:
            return state;
    }
};
export default tickets;
