import { StatusFilter, SortBy } from './actions';

export const initialState = {
    entities: {
        tickets: {
            byId: {},
        },
        threads: {
            byId: {},
        },
        users: {
            byId: {},
        },
    },
    ui: {
        common: {
            appName: 'TicketMe',
            token: null,
            currentUser: null,
            sidebarCollapsed: true,
        },
        tickets: {
            isLoading: false,
            statusFilter: StatusFilter.ALL,
            sortBy: SortBy.OLDEST,
            lockedTickets: [],
        },
    },
};
