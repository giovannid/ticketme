import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const StyledRouterButton = styled(Link)`
    display: ruby;
    align-items: center;
    border: none;
    background: #252531;
    height: 36px;
    line-height: 36px;
    border-radius: 4px;
    padding: 0 12px;
    color: #fff;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    font-size: 13px;
    opacity: 0.8;
    text-decoration: none;
    cursor: pointer;
    user-select: none;

    &:hover {
        opacity: 1;
    }

    &:active {
        opacity: 0.65;
    }

    svg {
        position: relative;
        top: 2px;
    }

    svg,
    span {
        flex-shrink: 0;
    }
`;
