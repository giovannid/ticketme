import React from 'react';
import { StyledRouterButton } from './style';

export default function RouterButton(props) {
    return <StyledRouterButton {...props} />;
}
