import styled from 'styled-components';

export const Div = styled.div`
    min-width: 20px;
    border-radius: 4px;
    background: #252531;
    height: 24px;
    line-height: 24px;
    padding: 0 6px;
    font-weight: 700;
    font-size: 11px;
    text-shadow: 0 0 1px rgba(0, 0, 0, 0.3);
`;

export const Red = styled(Div)`
    background: #e94f3e;
`;

export const Orange = styled(Div)`
    background: #ffa142;
`;

export const Blue = styled(Div)`
    background: #096dc1;
`;

export const Green = styled(Div)`
    background: #0bab53;
`;
