import React from 'react';
import { PriorityList } from '../../state/actions';
import { Div, Red, Orange, Blue, Green } from './style';

export default function PriorityLabel({ id, className }) {
    switch (parseInt(id)) {
        case 1:
            return <Red className={className}>{PriorityList[1]}</Red>;
        case 2:
            return <Orange className={className}>{PriorityList[2]}</Orange>;
        case 3:
            return <Blue className={className}>{PriorityList[3]}</Blue>;
        case 4:
            return <Green className={className}>{PriorityList[4]}</Green>;
        default:
            return <Div className={className} />;
    }
}
