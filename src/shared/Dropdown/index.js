import React, { useState, useEffect, useRef } from 'react';
import { StyledDropdown, Boundary, Name, Selected, Item, Menu } from './style';

export default function Dropdown(props) {
    const { onChange, options, selected = options[0], children } = props;
    const [toggle, setToggle] = useState(false);
    const parentDiv = useRef(null);

    const handleOptionclick = e => {
        e.stopPropagation();
        e.persist();
        setToggle(!toggle);
        onChange(e.target.dataset.name);
    };

    useEffect(() => {
        function detectOutsideClick(e) {
            if (
                parentDiv.current !== null &&
                !parentDiv.current.contains(e.target)
            ) {
                setToggle(false);
            }
        }
        window.addEventListener('click', detectOutsideClick);
        return () => window.removeEventListener('click', detectOutsideClick);
    }, []);

    return (
        <StyledDropdown ref={parentDiv} onClick={() => setToggle(!toggle)}>
            <Boundary>
                <Name active={toggle}>{children}</Name>
                <Selected active={toggle}>{selected.toLowerCase()}</Selected>
            </Boundary>
            {toggle ? (
                <Menu>
                    {options.map(el => (
                        <Item
                            selected={el === selected}
                            key={el}
                            data-name={el}
                            onClick={handleOptionclick}>
                            {el.toLowerCase()}
                        </Item>
                    ))}
                </Menu>
            ) : null}
        </StyledDropdown>
    );
}
