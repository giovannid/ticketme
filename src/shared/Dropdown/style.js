import styled from 'styled-components';

export const StyledDropdown = styled.div`
    text-transform: capitalize;
    font-size: 13px;
    cursor: pointer;
    position: relative;
    height: 36px;
    line-height: 36px;
    border-radius: 4px;
    background: #1f1f2a;
    display: flex;
    user-select: none;
`;

export const Boundary = styled.div`
    display: flex;
    &:active {
        opacity: 0.65;
    }
`;

export const Name = styled.span`
    border-radius: 4px 0 0 4px;
    padding: 0 15px;
    background: #252531;
    display: flex;
    align-items: center;
    height: 36px;
    line-height: 36px;
    opacity: ${props => (props.active ? 1 : 0.8)};
    ${StyledDropdown}:hover & {
        opacity: 1;
    }
    font-weight: 600;
`;

export const Selected = styled.span`
    color: #cc5600;
    padding: 0 15px;
    opacity: ${props => (props.active ? 1 : 0.8)};
    ${StyledDropdown}:hover & {
        opacity: 1;
    }
    font-weight: 600;
`;

export const Item = styled.span`
    color: #fff;
    line-height: 1em;
    padding: 11px 16px;
    font-weight: ${props => (props.selected ? 'bold' : 'initial')};
    background: ${props => (props.selected ? '#1f1f2a' : '#252531')};

    &:first-child {
        border-radius: 4px 4px 0 0;
    }

    &:last-child {
        border-radius: 0 0 4px 4px;
    }

    &:hover {
        background: #1f1f2a;
    }
`;

export const Menu = styled.div`
    min-width: 100%;
    margin-top: 5px;
    display: flex;
    flex-direction: column;
    position: absolute;
    left: 0;
    top: 100%;
    box-shadow: 0 2px 4px 0 rgba(34, 36, 38, 0.12),
        0 2px 10px 0 rgba(34, 36, 38, 0.15);
    z-index: 1;
`;
