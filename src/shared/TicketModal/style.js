import styled from 'styled-components';

export const StyledTicketModal = styled.div`
    z-index: 1000;
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    overflow: hidden auto;
`;

export const DarkBackground = styled.div`
    min-height: 100%;
    background: rgba(0, 0, 0, 0.54) none repeat scroll 0% 0%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: ${props =>
        props.isSidebarCollapsed ? '50px' : '50px 50px 50px 270px'};
    overflow: auto;
`;

export const TicketContainer = styled.div`
    display: flex;
    position: relative;
    width: ${props => props.customWidth || '100%'};
    background: #1b1b25;
    max-width: 1040px;
    border-radius: 3px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 10px 0px;
    justify-content: center;
    align-items: center;
    margin-bottom: 100px;
`;
