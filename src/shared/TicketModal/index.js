import React, { useRef, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { StyledTicketModal, DarkBackground, TicketContainer } from './style';

export default function TicketModal({ el: Element, ...rest }) {
    const { closeModal, width = '' } = rest;
    const ticketDiv = useRef(null);
    const isSidebarCollapsed = useSelector(
        state => state.ui.common.sidebarCollapsed
    );

    useEffect(() => {
        let container = document.getElementById('container');
        container.style.overflow = 'hidden';

        return () => {
            container.style.overflow = '';
        };
    }, []);

    useEffect(() => {
        function detectOutsideClick(e) {
            e.preventDefault();
            if (
                ticketDiv.current !== null &&
                !ticketDiv.current.contains(e.target) &&
                e.target.id !== 'collapseButton'
            ) {
                closeModal();
            }
        }
        window.addEventListener('click', detectOutsideClick);
        return () => window.removeEventListener('click', detectOutsideClick);
    }, [closeModal]);

    return (
        <StyledTicketModal>
            <DarkBackground isSidebarCollapsed={isSidebarCollapsed}>
                <TicketContainer ref={ticketDiv} customWidth={width}>
                    <Element {...rest} />
                </TicketContainer>
            </DarkBackground>
        </StyledTicketModal>
    );
}
