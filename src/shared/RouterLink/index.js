import React from 'react';
import { StyledRouterLink } from './style';

export default function RouterLink({ children, ...rest }) {
    return <StyledRouterLink {...rest}>{children}</StyledRouterLink>;
}
