import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const StyledRouterLink = styled(Link)`
    text-decoration: none;
    color: inherit;
`;
