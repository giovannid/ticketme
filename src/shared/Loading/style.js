import styled from 'styled-components';

export const StyledLoading = styled.div``;

export const Row = styled.div`
    display: grid;
    grid-template-columns: 50px 50px 4fr 120px 120px minmax(30px, 150px) 90px 50px;
`;

export const Column = styled.div`
    @keyframes fading {
        0% {
            opacity: 0.5;
        }

        50% {
            opacity: 0.9;
        }

        100% {
            opacity: 0.5;
        }
    }

    animation: fading 1.5s infinite;
    font-family: 'BLOKK Neue';
    font-size: 30px;
    line-height: 40px;
    color: rgb(47, 47, 68);
    animation: fading 1.5s infinite;
    display: flex;

    &.align-right {
        justify-content: right;
    }
`;
