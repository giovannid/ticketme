import React from 'react';
import { StyledLoading, Row, Column } from './style';

export default function Loading({ className }) {
    return (
        <StyledLoading className={className}>
            {[...Array(10).keys()].map((item, index) => (
                <Row key={index}>
                    <Column>WW</Column>
                    <Column>WW</Column>
                    <Column>WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW</Column>
                    <Column>WWWWW</Column>
                    <Column>WWWWW</Column>
                    <Column>WWWWWWW</Column>
                    <Column>WWWW</Column>
                    <Column className='align-right'>WW</Column>
                </Row>
            ))}
        </StyledLoading>
    );
}
