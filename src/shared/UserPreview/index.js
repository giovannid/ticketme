import React from 'react';
import { useSelector } from 'react-redux';
import { StyledUserPreview, Avatar, Name } from './style';

export default function UserPreview({
    id,
    displayName = true,
    onlyName = false,
    fullName = true,
    firstNameOnly = false,
    width = 27,
    height = 27,
    fontSize = 12,
    className,
}) {
    const user = useSelector(state => state.entities.users.byId[id]);
    return (
        <StyledUserPreview className={className}>
            {onlyName ? (
                <Name className='name'>
                    {firstNameOnly
                        ? user.firstName
                        : fullName
                        ? user.fullName
                        : user.nameAbbrev}
                </Name>
            ) : (
                <>
                    <Avatar
                        className='avatar'
                        style={
                            user.avatarUrl
                                ? { backgroundImage: `url(${user.avatarUrl})` }
                                : {}
                        }
                        width={width}
                        height={height}
                    />
                    {displayName ? (
                        <Name fontSize={fontSize} className='name'>
                            {firstNameOnly
                                ? user.firstName
                                : fullName
                                ? user.fullName
                                : user.nameAbbrev}
                        </Name>
                    ) : null}
                </>
            )}
        </StyledUserPreview>
    );
}
