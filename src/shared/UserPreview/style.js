import styled from 'styled-components';

export const StyledUserPreview = styled.div`
    display: flex;
    align-items: center;
`;
export const Avatar = styled.div`
    width: ${props => `${props.width}px`};
    height: ${props => `${props.height}px`};
    overflow: hidden;
    border-radius: 50%;
    background-position: top center;
    background-size: cover;
    background-color: #252531;
    margin-right: 7px;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border: 2px solid #fff;
    flex-shrink: 0;
`;

export const Name = styled.div`
    font-weight: 600;
    font-size: ${props => props.fontSize};
    overflow: hidden;
    text-overflow: ellipsis;
    flex-shrink: 1;
`;
