import React from 'react';
import { StatusFilter } from '../../state/actions';
import { StyledFilterOption, FilterIcon } from './style';

export default function FilterOption({ className, id }) {
    return (
        <StyledFilterOption className={className}>
            <FilterIcon />
            {StatusFilter[id].toLowerCase()}
        </StyledFilterOption>
    );
}
