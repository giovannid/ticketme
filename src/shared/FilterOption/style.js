import styled from 'styled-components';
import { FaFilter } from 'react-icons/fa';

export const StyledFilterOption = styled.div`
    display: flex;
    align-items: center;
    text-transform: capitalize;
    font-size: 14px;
    font-weight: 600;
    width: 100%;
`;

export const FilterIcon = styled(FaFilter)`
    margin-top: 2px;
    margin-right: 7px;
    font-size: 12px;
`;
