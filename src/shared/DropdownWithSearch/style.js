import styled from 'styled-components';

export const StyledDropdown = styled.div`
    font-size: 13px;
    position: relative;
    display: inline-block;
    width: 100%;
    margin-bottom: 15px;
`;

export const Name = styled.div`
    font-weight: 700;
    color: #6a6a7d;
    font-size: 12px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

export const Selected = styled.div`
    color: #fff;
    background: ${props =>
        props.readOnly || props.noStyle ? 'none' : '#252531'};
    height: ${props => (props.noStyle ? 'initial' : '36px')};
    line-height: ${props => (props.noStyle ? 'initial' : '36px')};
    padding: ${props => (props.readOnly || props.noStyle ? '0' : '0 12px;')};
    border-radius: 4px;
    opacity: ${props => (!props.active ? 1 : 0.8)};
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    cursor: ${props => (props.readOnly ? 'initial' : 'pointer')};
    user-select: none;
    display: inline-flex;
    align-items: center;
`;

export const Menu = styled.div`
    min-width: 100%;
    margin-top: 5px;
    display: flex;
    flex-direction: column;
    position: absolute;
    left: 0;
    top: 100%;
    box-shadow: 0 2px 4px 0 rgba(34, 36, 38, 0.12),
        0 2px 10px 0 rgba(34, 36, 38, 0.15);
    z-index: 1;
    background: #252531;
    color: #fff;
    border-radius: 4px;
    width: 100%;
    min-width: 200px;
`;

export const SearchBox = styled.input`
    background: none;
    border: none;
    color: #fff;
    height: 40px;
    line-height: 40px;
    padding: 0 14px;
    width: 100%;
    font-size: 14px;
    font-weight: 600;

    &::placeholder {
        color: #89899f;
        opacity: 1;
        font-size: 14px;
        font-weight: 600;
    }
`;

export const List = styled.div`
    max-height: 200px;
    overflow: hidden auto;
`;

export const Text = styled.div`
    color: #89899f;
    font-weight: 600;
`;

export const Item = styled.div`
    height: 40px;
    line-height: 40px;
    padding: 0 14px;
    font-size: 14px;
    display: flex;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
    align-items: center;

    .el {
        pointer-events: none;
    }

    &:hover {
        background: #1f1f2a;
    }

    &.loading,
    &.empty {
        cursor: initial;
        color: #89899f;

        &:hover {
            background: none;
        }
    }
`;

export const Icon = styled.div`
    display: flex;
    align-items: center;
    margin-right: 6px;

    svg {
        color: #89899f;
        font-size: 14px;
    }
`;
