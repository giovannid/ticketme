import React, { useState, useEffect, useRef } from 'react';
import { LoadingIcon } from '../../style';
import {
    StyledDropdown,
    Name,
    Selected,
    Menu,
    SearchBox,
    List,
    Text,
    Item,
    Icon,
} from './style';

export default function DropdownWithSearch(props) {
    const {
        accessor,
        searchAttr,
        label = '',
        options,
        selected = '',
        defaultOption = '',
        isFetching,
        element: Element,
        readOnly,
        noStyle = false,
        onChange = () => {},
        className,
    } = props;

    const [toggle, setToggle] = useState(false);
    const [selectedOption, setSelectedOption] = useState('');
    const [searchKeywords, setSearchKeywords] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const parentDiv = useRef(null);

    useEffect(() => {
        if (selected === undefined || selected === '') {
            setSelectedOption(defaultOption);
        } else {
            setSelectedOption(selected);
        }
    }, [selected, defaultOption]);

    const handleSelectedClick = () => {
        if (!readOnly) setToggle(!toggle);
    };

    const handleOptionClick = e => {
        e.stopPropagation();
        setToggle(!toggle);
        if (e.target.dataset.accessor !== `${selectedOption}`) {
            onChange(e.target.dataset.accessor);
            setSelectedOption(e.target.dataset.accessor);
        }
    };

    useEffect(() => {
        if (searchKeywords === '') setSearchResults([]);
        const results = options.filter(
            option =>
                option[searchAttr]
                    .toUpperCase()
                    .indexOf(searchKeywords.toUpperCase()) > -1
        );
        setSearchResults(results);
    }, [searchKeywords, options, searchAttr]);

    useEffect(() => {
        function detectOutsideClick(e) {
            if (
                toggle === true &&
                parentDiv.current !== null &&
                !parentDiv.current.contains(e.target)
            ) {
                setToggle(false);
            }
        }
        window.addEventListener('click', detectOutsideClick);
        return () => window.removeEventListener('click', detectOutsideClick);
    }, [toggle]);

    const displayElements = arr =>
        arr.map(option => (
            <Item
                key={option.id}
                data-accessor={option[accessor]}
                onClick={handleOptionClick}>
                <Element
                    {...{ [accessor]: option[accessor] }}
                    className='el'></Element>
            </Item>
        ));

    return (
        <StyledDropdown className={className}>
            {label.length > 0 ? <Name active={toggle}>{label}</Name> : null}
            <Selected
                active={toggle}
                readOnly={readOnly}
                noStyle={noStyle}
                onClick={handleSelectedClick}
                className='elementContainer'>
                {selectedOption === '' ? (
                    <Text className='element'>Select</Text>
                ) : (
                    <Element
                        className='element'
                        {...{ [accessor]: selectedOption }}
                    />
                )}
            </Selected>
            {toggle ? (
                <Menu ref={parentDiv}>
                    <SearchBox
                        value={searchKeywords}
                        onChange={e => setSearchKeywords(e.target.value)}
                        placeholder='Search'
                        autoFocus
                    />
                    <List>
                        {!isFetching &&
                            options.length > 0 &&
                            displayElements(
                                searchKeywords.length > 0
                                    ? searchResults
                                    : options
                            )}
                    </List>
                    {isFetching ? (
                        <Item className='loading'>
                            <Icon>
                                <LoadingIcon />
                            </Icon>{' '}
                            Loading
                        </Item>
                    ) : null}
                    {!isFetching &&
                    (options.length === 0 || searchResults.length === 0) ? (
                        <Item className='empty'>No results</Item>
                    ) : null}
                </Menu>
            ) : null}
        </StyledDropdown>
    );
}
