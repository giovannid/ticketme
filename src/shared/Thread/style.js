import styled from 'styled-components';
import UserPreview from '../UserPreview';

export const StyledThread = styled.div`
    width: 100%;
    display: flex;
    margin: 30px 0;
    align-items: flex-start;
`;

export const ProfilePic = styled(UserPreview)`
    margin-right: 13px;
    .avatar {
        margin-right: 0;
    }
`;

export const Content = styled.div`
    width: 100%;
`;

export const TopInfo = styled.div`
    display: flex;
    height: 32px;
    align-items: center;
`;
export const Name = styled(UserPreview)`
    .name {
        height: 32px;
        line-height: 32px;
        font-size: 15px;
    }
`;

export const Date = styled.div`
    margin-left: 13px;
    color: #89899f;
    font-size: 15px;
    font-weight: 600;
`;

export const Body = styled.div`
    width: 100%;
    margin: 5px 0 0 0;
    padding: 10px 15px;
    font-size: 14px;
    color: #f8f8ff;
    line-height: 21px;
    white-space: pre-wrap;
    background: ${props => (props.agentReply ? '#954a13;' : '#1f1f2a')};
    border-radius: 4px;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
`;

export const AgentType = styled.div`
    color: #89899f;
    font-size: 10px;
    font-weight: 400;
    margin-top: 10px;
    display: flex;
    justify-content: flex-end;
`;
