import React from 'react';
import { useSelector } from 'react-redux';
import { LoadingIcon } from '../../style';
import * as mtwitter from 'moment-twitter';
import moment from 'moment/moment';
import { UserType } from '../../state/actions';
import {
    StyledThread,
    ProfilePic,
    Content,
    TopInfo,
    Name,
    Date,
    Body,
    AgentType,
} from './style';

export default function Thread({ id, className }) {
    const thread = useSelector(state => state.entities.threads.byId[id]);
    const user = useSelector(state => state.entities.users.byId[thread.user]);

    if (!thread) return <LoadingIcon />;

    return (
        <StyledThread className={className}>
            <ProfilePic
                width={32}
                height={32}
                id={thread.user}
                displayName={false}
            />
            <Content>
                <TopInfo>
                    <Name id={thread.user} onlyName={true} fullName={true} />
                    <Date title={moment(thread.createdAt).toString()}>
                        {mtwitter(thread.createdAt).twitter()} ago
                    </Date>
                </TopInfo>
                <Body agentReply={user.type === UserType.STAFF}>
                    {thread.body}
                </Body>
                <AgentType>
                    {user.type === UserType.STAFF
                        ? 'Staff reply'
                        : 'User reply'}
                </AgentType>
            </Content>
        </StyledThread>
    );
}
