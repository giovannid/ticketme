import React from 'react';
import {
    StyledLoading,
    Left,
    Right,
    Label,
    Top,
    Subject,
    Thread,
    ProfilePic,
    Reply,
} from './style';

export default function LoadingTicket({ className }) {
    return (
        <StyledLoading className={className}>
            <Left>
                <Top width='30%' />
                <Subject width='80%' />
                <Subject width='60%' />
                <Thread>
                    <ProfilePic />
                    <Reply />
                </Thread>
                <Thread>
                    <ProfilePic />
                    <Reply />
                </Thread>
            </Left>
            <Right>
                <Top width='25%' className='alignRight' />
                <Label />
                <Top />
                <Label />
                <Top />
                <Label />
                <Top />
                <Label />
                <Top />
            </Right>
        </StyledLoading>
    );
}
