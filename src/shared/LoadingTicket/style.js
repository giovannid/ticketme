import styled from 'styled-components';

export const StyledLoading = styled.div`
    display: flex;
`;

const Block = styled.div`
    @keyframes fading {
        0% {
            opacity: 0.5;
        }

        50% {
            opacity: 0.9;
        }

        100% {
            opacity: 0.5;
        }
    }

    background: rgb(47, 47, 68);
    animation: fading 1.5s infinite;
    display: flex;
    border-radius: 4px;

    &.alignRight {
        align-self: flex-end;
    }
`;

export const Left = styled.div`
    width: 70%;
    padding-right: 50px;
`;

export const Right = styled.div`
    width: 30%;
    display: flex;
    flex-direction: column;
`;

export const Label = styled(Block)`
    height: 15px;
    width: 50%;
    margin-bottom: 10px;
`;

export const Top = styled(Block)`
    height: 25px;
    margin-bottom: 20px;
    width: ${props => props.width || 'initial'};
`;

export const Subject = styled(Block)`
    height: 30px;
    margin-bottom: 10px;
    width: ${props => props.width || 'initial'};
`;

export const Thread = styled.div`
    display: flex;
    margin: 20px 0;

    &:last-child {
        margin-bottom: 0;
    }
`;

export const ProfilePic = styled(Block)`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    margin-right: 15px;
    flex-shrink: 0;
`;

export const Reply = styled(Block)`
    width: 100%;
    height: 100px;
    background: rgb(47, 47, 68);
`;
