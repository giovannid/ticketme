import styled from 'styled-components';

export const StyledButton = styled.button`
    display: flex;
    align-items: center;
    border: none;
    background: #252531;
    height: 36px;
    line-height: 36px;
    border-radius: 4px;
    padding: 0 12px;
    color: #fff;
    font-weight: 600;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    font-size: 13px;
    opacity: 0.8;
    cursor: pointer;
    user-select: none;

    &:hover {
        opacity: 1;
    }

    &:active {
        opacity: 0.65;
    }
`;
