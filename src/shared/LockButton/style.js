import styled from 'styled-components';
import Button from '../Button';
import { FaLock } from 'react-icons/fa';

export const StyledLockButton = styled(Button)``;

export const User = styled.div`
    margin-left: -4px;
    margin-right: -2px;

    .name {
        margin-right: 10px;
    }
`;

export const TimeLocked = styled.div`
    overflow: hidden;

    ${StyledLockButton}:hover & {
        height: 0;
        width: 0;
        opacity: 0;
        transition: opacity 0.5s ease-out;
    }
`;

export const Unlock = styled.div`
    overflow: hidden;
    height: 0;
    width: 0;
    opacity: 0;

    ${StyledLockButton}:hover & {
        height: auto;
        width: auto;
        opacity: 1;
        display: flex;
        align-items: center;

        svg {
            flex-shrink: 0;
            padding-right: 3px;
        }
    }
    transition: opacity 0.3s ease-in;
`;

export const Lock = styled.div`
    display: flex;
    align-items: center;
`;

export const LockIcon = styled(FaLock)`
    margin-right: 5px;
`;
