import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UserPreview from '../UserPreview';
import moment from 'moment-twitter';
import { lockTickets, unlockTickets } from '../../state/actions';
import { FaLockOpen } from 'react-icons/fa';
import {
    StyledLockButton,
    User,
    TimeLocked,
    Unlock,
    Lock,
    LockIcon,
} from './style';

export default function LockButton({
    lockedBy,
    ticket,
    className,
    userPreview = true,
    displayName = false,
}) {
    const currentUser = useSelector(state => state.ui.common.currentUser);
    const dispatch = useDispatch();

    const handleClick = e => {
        e.stopPropagation();

        if (lockedBy) {
            dispatch(unlockTickets([ticket.id]));
        } else {
            dispatch(lockTickets([ticket.id], currentUser));
        }
    };

    return (
        <StyledLockButton className={className} onClick={handleClick}>
            {lockedBy ? (
                <>
                    {userPreview ? (
                        <User>
                            <UserPreview
                                id={lockedBy.userId}
                                displayName={displayName}
                            />
                        </User>
                    ) : null}
                    <TimeLocked>
                        {moment(lockedBy.lockedAt).twitter()}
                    </TimeLocked>
                    <Unlock>
                        <FaLockOpen size={16} /> Unlock
                    </Unlock>
                </>
            ) : (
                <Lock>
                    <LockIcon size={13} /> Lock
                </Lock>
            )}
        </StyledLockButton>
    );
}
