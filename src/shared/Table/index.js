import React, { useState, useEffect } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { FaRegEye, FaTimes } from 'react-icons/fa';
import RouterButton from '../RouterButton';
import {
    StyledTable,
    Row,
    HeaderRowItem,
    RowItem,
    Checkbox,
    StyledLoading,
    ASCIcon,
    DESCIcon,
    MultiSelectionPanel,
    MultiSelItem,
    ItemCount,
    Empty,
} from './style';

const sortList = (list, options) => {
    const { key = 'id', order = 'asc', asString = false } = options;
    return [...list].sort((a, b) => {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }

        const _a = asString ? a[key].toUpperCase() : parseInt(a[key]);
        const _b = asString ? b[key].toUpperCase() : parseInt(b[key]);

        let comparison = _a > _b ? 1 : -1;
        return order === 'desc' ? comparison * -1 : comparison;
    });
};

export default function Table({
    className,
    data,
    columns,
    sortBy,
    isFetching,
    multiSelectionMenu,
    rowClasses = () => {},
}) {
    const _data = React.useMemo(() => sortList(data, sortBy), [data, sortBy]);
    const [selectedItems, setSelectedItems] = useState([]);
    const [isMainCbSelected, setMainCbSelected] = useState(false);
    const match = useRouteMatch();

    const handleItemClick = e => {
        const value = parseInt(e.target.value);
        if (selectedItems.includes(value)) {
            setSelectedItems(items => items.filter(id => id !== value));
        } else {
            setSelectedItems(items => [...items, value]);
        }
    };

    const handleMainCheckboxClick = e => {
        if (e.target.checked) {
            const items = _data.map(item => parseInt(item.id));
            setSelectedItems(items);
            setMainCbSelected(true);
        } else {
            setSelectedItems([]);
            setMainCbSelected(false);
        }
    };

    const closeMultiSelectionPanel = () => {
        setMainCbSelected(false);
        setSelectedItems([]);
    };

    useEffect(() => {
        closeMultiSelectionPanel();
    }, [data]);

    return (
        <StyledTable className={className}>
            <Row className='thead'>
                <HeaderRowItem key='checkbox'>
                    <Checkbox
                        type='checkbox'
                        checked={isMainCbSelected}
                        onChange={handleMainCheckboxClick}
                    />
                </HeaderRowItem>
                {columns.map(column => (
                    <HeaderRowItem key={column.title} {...column}>
                        {column.title}
                        {column.sortOrder !== false ? (
                            column.sortOrder === 'asc' ? (
                                <ASCIcon />
                            ) : (
                                <DESCIcon />
                            )
                        ) : null}
                    </HeaderRowItem>
                ))}
                <RowItem key='details' />
            </Row>

            {isFetching ? <StyledLoading /> : null}

            {!isFetching && _data.length === 0 ? (
                <Empty>No results found.</Empty>
            ) : null}

            {_data.map(row => (
                <Row
                    key={row.id}
                    className={`row ${rowClasses(row)}`}
                    {...{
                        selected: selectedItems.includes(parseInt(row.id))
                            ? true
                            : false,
                    }}>
                    <RowItem
                        key={`checkbox${row.id}`}
                        className={`column column-checkbox`}>
                        <Checkbox
                            type='checkbox'
                            value={row.id}
                            checked={selectedItems.includes(parseInt(row.id))}
                            onChange={handleItemClick}
                        />
                    </RowItem>

                    {columns.map(column => (
                        <RowItem
                            key={column.title}
                            className={`column column-${column.accessor}`}>
                            <span>
                                {column.transformValue
                                    ? column.transformValue(
                                          row[column.accessor],
                                          row
                                      )
                                    : row[column.accessor]}
                            </span>
                        </RowItem>
                    ))}

                    <RowItem
                        key={`details${row.id}`}
                        className={`column column-details`}>
                        <RouterButton
                            to={`${match.url}/${row.id}`}
                            style={{ marginLeft: 'auto' }}>
                            <FaRegEye />
                        </RouterButton>
                    </RowItem>
                </Row>
            ))}

            {selectedItems.length > 0 ? (
                <MultiSelectionPanel>
                    <ItemCount>{selectedItems.length} selected</ItemCount>
                    {multiSelectionMenu.map((item, index) => (
                        <MultiSelItem
                            onClick={() => item.onClick(selectedItems)}
                            key={index}>
                            {item.content}
                        </MultiSelItem>
                    ))}
                    <MultiSelItem
                        onClick={closeMultiSelectionPanel}
                        className='close'>
                        <FaTimes />
                    </MultiSelItem>
                </MultiSelectionPanel>
            ) : null}
        </StyledTable>
    );
}
