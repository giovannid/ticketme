import styled from 'styled-components';
import { FaSortAmountDown, FaSortAmountUpAlt } from 'react-icons/fa';
import Loading from '../Loading';

export const StyledTable = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
`;

export const Row = styled.div`
    display: grid;
    grid-template-columns:
        50px minmax(30px, 50px) 4fr 120px 120px minmax(30px, 150px)
        90px 50px;

    &:first-child {
        height: 50px;
        border-bottom: 2px solid #2e2e44;
        padding: 0;
        border-radius: 0;
        margin: 0 20px 10px 20px;
    }

    &:hover:first-child {
        background: transparent;
    }

    background: ${props => (props.selected ? '#1f1f2a' : 'transparent')};
    box-shadow: ${props =>
        props.selected ? '0 1px 2px rgba(0,0,0,0.05)' : 'initial'};
    margin-bottom: 5px;
    height: 60px;
    padding: 0 20px;
    border-radius: 4px;
`;

export const HeaderRowItem = styled.div`
    color: rgb(110, 110, 136);
    font-size: 11px;
    text-transform: uppercase;
    font-weight: 700;
    color: #6a6a7d;
    display: flex;
    align-items: center;
    user-select: none;

    &.pointer {
        cursor: pointer;
    }
`;

export const RowItem = styled.div`
    display: flex;
    align-items: center;
    font-size: 13px;
    font-weight: 600;
`;

export const Checkbox = styled.input`
    appearance: none;
    background-color: transparent;
    border: 1px solid #2e2e44;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05),
        inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
    padding: 9px !important;
    border-radius: 3px;
    display: block;
    position: relative;
    cursor: pointer;

    &:checked {
        background-color: #4f2506;
        border: 1px solid #cc5600;
    }
`;

export const StyledLoading = styled(Loading)`
    margin: 0 20px;
`;

export const ASCIcon = styled(FaSortAmountUpAlt)`
    margin-left: 3px;
    fill: #cc5600;
`;

export const DESCIcon = styled(FaSortAmountDown)`
    margin-left: 3px;
    fill: #cc5600;
`;

export const MultiSelectionPanel = styled.div`
    position: fixed;
    bottom: 30px;
    left: 50%;
    transform: translate(-50%, 0);
    min-width: 30px;
    height: 45px;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 4px;
    display: flex;
`;

export const MultiSelItem = styled.button`
    min-width: 50px;
    font-size: 13px;
    font-weight: 600;
    border: 0;
    color: #fff;
    background: #2a2a35;
    cursor: pointer;
    height: 45px;
    display: flex;
    align-items: center;
    padding: 0 10px;
    justify-content: center;

    svg {
        padding-right: 7px;
        flex-shrink: 0;
    }

    &:active {
        background: #252531;
    }

    &.close {
        border-radius: 0 4px 4px 0;
        padding: 0;
        background: #353642;

        &:active {
            background: #2d2e3c;
        }

        svg {
            padding: 0;
        }
    }
`;

export const ItemCount = styled.div`
    background: #353642;
    border-radius: 4px 0 0 4px;
    height: 45px;
    display: flex;
    align-items: center;
    font-size: 14px;
    font-weight: 600;
    color: #fff;
    padding: 0 10px;
    user-select: none;
`;

export const Empty = styled.div`
    color: #6a6a7d;
    text-align: center;
    font-weight: 600;
    padding: 10px 0;
`;
