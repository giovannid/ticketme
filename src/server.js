import {
    Server,
    Model,
    Factory,
    belongsTo,
    hasMany,
    RestSerializer,
    trait,
} from 'miragejs';
import faker from 'faker';
import moment from 'moment/moment';

const ApplicationSerializer = RestSerializer.extend({});

const randomStatus = () => {
    const status = ['CLOSED', 'OPEN', 'ANSWERED', 'UNANSWERED'];
    return status[Math.floor(Math.random() * 4)];
};

const randomNumber = (min, max) => {
    return Math.floor(Math.random() * max) + min;
};

new Server({
    serializers: {
        application: ApplicationSerializer,
        ticket: ApplicationSerializer.extend({
            include: ['threads'],
        }),
        threadsAndTicket: ApplicationSerializer.extend({
            include: ['threads', 'ticket'],
        }),
        thread: ApplicationSerializer.extend({
            include: ['ticket', 'user'],
        }),
    },
    models: {
        user: Model,
        ticket: Model.extend({
            threads: hasMany(),
            assignedTo: belongsTo('user'),
        }),
        thread: Model.extend({
            ticket: belongsTo(),
            user: belongsTo(),
        }),
    },
    factories: {
        ticket: Factory.extend({
            subject: () => faker.lorem.sentence(randomNumber(5, 13)),
            status: () => randomStatus(),
            priority: () => randomNumber(1, 4),
            lockedBy: () => false,
            afterCreate(ticket, server) {
                const agent = server.create('user', 'staffType');
                const user = server.create('user', 'userType');

                const agentThreads = server.createList(
                    'thread',
                    Math.floor(Math.random() * 5) + 1,
                    { ticket, user: agent }
                );

                const userThreads = server.createList(
                    'thread',
                    Math.floor(Math.random() * 5) + 1,
                    { ticket, user }
                );

                const threads = [userThreads, agentThreads]
                    .reduce((r, a) => {
                        a.map((a, i) => (r[i] = r[i] || []).push(a));
                        return r;
                    }, [])
                    .reduce((a, b) => a.concat(b));

                ticket.update({
                    assignedTo: agent,
                    threads: threads,
                    lastUpdated: threads[threads.length - 1].createdAt,
                    createdAt: threads[0].createdAt,
                    lockedBy: randomNumber(0, 2)
                        ? {
                              userId: randomNumber(0, 2) ? agent.id : '1',
                              lockedAt: faker.date.recent(),
                          }
                        : false,
                });
            },
        }),
        thread: Factory.extend({
            body: () => faker.lorem.paragraph(),
            attachments: () => [],
            createdAt: i => {
                const date = moment().subtract(i, 'h');
                return date.toISOString();
            },
        }),
        user: Factory.extend({
            firstName: () => faker.name.firstName(),
            lastName: () => faker.name.lastName(),
            staffType: trait({
                type: 'STAFF',
            }),
            userType: trait({
                type: 'USER',
            }),
            createdAt: () => faker.date.recent(),
            avatarUrl: () => faker.image.avatar(),
            fullName() {
                return `${this.firstName} ${this.lastName}`;
            },
            nameAbbrev() {
                return `${this.firstName} ${this.lastName[0]}.`;
            },
        }),
    },
    seeds(server) {
        server.create('user', 'staffType', {
            id: 1,
            firstName: 'Admin',
            lastName: 'User',
        });
        server.createList('ticket', 15);
    },
    routes() {
        this.namespace = 'api';
        this.resource('tickets');
        this.resource('threads');
        this.resource('users');

        this.get('users/bytype/:usertype', (schema, request) => {
            const type = request.params.usertype.toUpperCase();
            return schema.users.where({ type });
        });

        this.get('tickets/bystatus/:status', (schema, request) => {
            const status = request.params.status.toUpperCase();
            if (status === 'ALL') {
                return schema.tickets.all();
            }
            return schema.tickets.where({ status });
        });

        this.get('tickets/lockedby/:userId', (schema, request) => {
            const userId = request.params.userId;
            return schema.tickets.where(
                ticket => ticket.lockedBy.userId === userId
            );
        });

        this.post('tickets', function (schema, request) {
            let attrs = JSON.parse(request.requestBody);

            let user = schema.users.find(attrs.user);
            let assignedTo = schema.users.find(attrs.ticket.assignedTo);
            let thread = schema.threads.create({
                body: attrs.thread.body,
                user,
            });
            let ticket = schema.tickets.create({
                subject: attrs.ticket.subject,
                status: attrs.ticket.status,
                priority: attrs.ticket.priority,
                lockedBy: false,
                createdAt: moment().toISOString(),
                lastUpdated: moment().toISOString(),
                threads: [thread],
                assignedTo: assignedTo,
            });
            return this.serialize(ticket, 'threadsAndTicket');
        });

        this.post('tickets/lock', (schema, request) => {
            let attrs = JSON.parse(request.requestBody);
            let user = schema.users.find(attrs.userId);
            let tickets = schema.tickets.find(attrs.ids);

            return tickets.update({
                lockedBy: { userId: user.id, lockedAt: moment().toISOString() },
            });
        });

        this.post('tickets/unlock', (schema, request) => {
            let attrs = JSON.parse(request.requestBody);
            let tickets = schema.tickets.find(attrs.ids);

            return tickets.update({
                lockedBy: false,
            });
        });

        this.patch('tickets/:id', (schema, request) => {
            let attrs = JSON.parse(request.requestBody);
            let id = request.params.id;
            let ticket = schema.tickets.find(id);
            if (attrs.assignedTo) {
                attrs.assignedTo = schema.users.find(attrs.assignedTo);
            }
            attrs.lastUpdated = moment().toISOString();

            return ticket.update(attrs);
        });

        this.delete('tickets/:id', function (schema, request) {
            let id = request.params.id;
            let ticket = schema.tickets.find(id);
            ticket.threads.destroy();
            ticket.destroy();
            return this.serialize(ticket, 'threadsAndTicket');
        });

        this.post('tickets/reply', function (schema, request) {
            let attrs = JSON.parse(request.requestBody);
            let user = schema.users.find(attrs.userId);
            let ticket = schema.tickets.find(attrs.ticketId);

            schema.threads.create({
                body: attrs.body,
                createdAt: moment().toISOString(),
                user,
                ticket,
            });

            ticket.update({
                assignedTo: user,
                lastUpdated: moment().toISOString(),
                lockedBy: false,
                status: 'ANSWERED',
            });

            return this.serialize(ticket, 'threadsAndTicket');
        });
    },
});
