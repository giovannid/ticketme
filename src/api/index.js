import axios from 'axios';
import {
    normalizeTickets,
    normalizeTicket,
    normalizeUser,
    normalizeUsers,
} from './normalize';

export async function fetchUsers(userType) {
    try {
        const response = await axios.get(`/api/users/bytype/${userType}`);
        return normalizeUsers(response.data);
    } catch (error) {}
}

export async function fetchTickets(status) {
    try {
        const response = await axios.get(`/api/tickets/bystatus/${status}`);
        return normalizeTickets(response.data);
    } catch (error) {}
}

export async function fetchLockedTickets(userId) {
    try {
        const response = await axios.get(`/api/tickets/lockedby/${userId}`);
        return normalizeTickets(response.data);
    } catch (error) {}
}

export async function fetchTicket(id) {
    try {
        const response = await axios.get(`/api/tickets/${id}`);
        return normalizeTicket(response.data);
    } catch (error) {}
}

export async function createTicket(data) {
    try {
        const response = await axios.post(`/api/tickets`, data);
        return normalizeTicket(response.data);
    } catch (error) {}
}

export async function updateTicket(id, data) {
    try {
        const response = await axios.patch(`/api/tickets/${id}`, data);
        return normalizeTicket(response.data);
    } catch (error) {}
}

export async function deleteTicket(id) {
    try {
        const response = await axios.delete(`/api/tickets/${id}`);
        return normalizeTicket(response.data);
    } catch (error) {}
}

export async function unlockTickets(arrIds) {
    try {
        const response = await axios.post(`/api/tickets/unlock`, {
            ids: arrIds,
        });
        return normalizeTickets(response.data);
    } catch (error) {}
}

export async function lockTickets(arrIds, userId) {
    try {
        const response = await axios.post(`/api/tickets/lock`, {
            ids: arrIds,
            userId,
        });
        return normalizeTickets(response.data);
    } catch (error) {}
}

export async function currentUser(token = null) {
    try {
        const response = await axios.get(`/api/users/1`);
        return normalizeUser(response.data);
    } catch (error) {}
}

export async function replyTicket(userId, ticketId, body) {
    try {
        const response = await axios.post(`/api/tickets/reply`, {
            userId,
            ticketId,
            body,
        });
        return normalizeTicket(response.data);
    } catch (error) {}
}
