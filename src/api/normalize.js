import { normalize, schema } from 'normalizr';

const user = new schema.Entity('users');
const thread = new schema.Entity('threads', {
    user,
});
const ticket = new schema.Entity('tickets', {
    assignedTo: user,
    threads: [thread],
});
thread.define({ ticket });

const multipleTickets = { tickets: [ticket], users: [user], threads: [thread] };
const singleTicket = { ticket, users: [user], threads: [thread] };
const singleUser = { user };
const multipleUsers = { users: [user] };

export const normalizeTickets = data => normalize(data, multipleTickets);
export const normalizeTicket = data => normalize(data, singleTicket);
export const normalizeUser = data => normalize(data, singleUser);
export const normalizeUsers = data => normalize(data, multipleUsers);
